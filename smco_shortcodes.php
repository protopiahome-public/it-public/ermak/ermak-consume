<?php
	// shortcodes
	add_action("init", "smco_add_shortcodes");
	function smco_add_shortcodes()
	{
		add_shortcode('smco_user_consume', 	'smco_user_consume'); 
	
	}
	function smco_user_consume()
	{
		
		if(!is_user_logged_in())
		{
			return "<div class='smp-comment'>".__("You must logged in!", 'smp')."<BR>
			<a href='".wp_login_url( home_url())."' title='Login'>".__('Login', 'smc')."</a></div>";
		}
		
		global $user_iface_color;	
		global $current_goods_type;
		global $Soling_Metagame_Constructor;	
		global $Ermak_Consume;
		global $all_goods_types;
		$all_goods_types	= Goods_Type::get_global();	
		$options					= get_option(SMCo);
		//$options['supported_types']['user'];
		
		//
		$meta_query					= array();
		$meta_query['relation']		= 'AND';
		$meta_query[]				= array(
												'key'	=> 'consumer_type',
												'value'	=> 'user'
											);
		$meta_query[]				= array(
												'key'	=> 'consumer_id',
												'value'	=> get_current_user_id()
											);
		$arg		= array(
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'orderby'  		=> 'title',
									'order'     	=> 'ASC',
									'post_type' 	=> GOODS_BATCH_NAME,
									'post_status' 	=> 'publish',
									'meta_query'	=> $meta_query,
								);
		$all_goods_batchses	= get_posts($arg);
		
		
		//==========================
		//
		//from server
		//
		//==========================
		
		$html				= SMP_Goods_Batch::serverside_service($all_goods_batchses);
		
		
		//var_dump ($meta_query);
		//echo "<BR>";
		//var_dump (count($all_goods_batchses));
		$bs				= '<h3>'. __('Reserves','smco').'</h3>';
		foreach($all_goods_batchses as $batch_id)
		{
			$bgb		= SMP_Goods_Batch::get_instance($batch_id->ID);
			if($bgb)
				$bs		.= $bgb->get_stroke($batch_id, 1);
			else
				$bs		.= "<h3>". $batch_id. "</h3>";
		}
		
		
		
		$consume_scheme_id	= get_user_meta(get_current_user_id(),"consume");
		$consume_scheme_id	= $consume_scheme_id[0];	
		$requirements		= "
		<h3>".__("Personal Requirements", "smco"). "</h3>
		<div>".
			$Ermak_Consume->admin_consume_form($consume_scheme_id).
		"</div>";
		
		
		//report
		$reports_text		= "<h3>".__("Reports", "smco")."</h3>";
		$report_id			= SMCO_Report::get_user_report_id(get_user_by('id', get_current_user_id()));
		$report				= get_post($report_id);
		$reports_text		.= $report->post_content;
		
		$html				= "
		".
		Assistants::get_tabs(
									array(	
											apply_filters("smpo_butch_slide_1", array("title" => __("Reserves", "smco"), 				"slide" => $bs)),	
											apply_filters("smpo_butch_slide_2", array("title" => __("Reports", "smco"), 				"slide" => $reports_text)),
											apply_filters("smpo_butch_slide_0", array("title" => __("Personal Requirements", "smco"),	"slide" => $requirements)),									
											apply_filters("smpo_butch_slide_3", array("title" => __("Requirements Calculator", "smco"),	"slide" => "")),									
										),
										'user_consume_'
								).	
		"<div>";
		
		$html				.= "</div>
		";
		echo $html;
	}
?>