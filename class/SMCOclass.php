<?php 

	class Ermak_Consume
	{
		public $options;
		public $i=0;
		function __construct()
		{
			$factory_type			= SMP_FACTORY;
			//return;
			add_action( 'admin_menu',										array($this, 'admin_page_handler'), 4);
			add_action( 'admin_menu',										array($this, 'admin_subpage_handler'), 99);
			add_action( 'admin_enqueue_scripts', 							array($this, 'admin_scripts') );	
			add_action( 'wp_enqueue_scripts', 								array($this, 'front_script_add') );
			
			// Consume Scheme
			add_action( 'init', 											array($this, 'add_cs_type'), 35 );
			add_action( 'save_post_'.SMCO_CONSUME_SCHEME_TYPE,				array($this, 'true_cs_save_box_data'));
			add_action( 'admin_menu',										array($this, 'my_cs_extra_fields'), 5);
			add_filter( 'manage_edit-'.SMCO_CONSUME_SCHEME_TYPE.'_columns',	array($this, 'add_cs_views_column'), 4);
			add_filter( 'manage_edit-'.SMCO_CONSUME_SCHEME_TYPE.'_sortable_columns', array($this, 'add_cs_views_sortable_column'));
			add_filter( 'manage_'.SMCO_CONSUME_SCHEME_TYPE.'_posts_custom_column', 	array($this, 'fill_cs_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php	
			add_action('smc_myajax_submit', 								array($this, 'smc_myajax_submit'));
			add_action('smc_myajax_admin_submit', 							array($this, 'smc_myajax_admin_submit'), 15);
			
			add_action( 'smp_batch_extra_fields2',							array($this, 'smp_batch_extra_fields'), 12, 2);
			add_action('save_post_goods_batch',								array($this, 'true_save_goods_batch_box_data'),12);
			add_filter('manage_edit-goods_batch_columns',		 			array($this, 'add_views_column_batch'), 5);
			add_filter('manage_edit-goods_batch_sortable_columns', 			array($this,'add_views_sortable_column_batch'), 11);
			add_filter('manage_goods_batch_posts_custom_column', 			array($this,'fill_views_column_batch'), 6, 2); 
			
			$this->options							= get_option(SMCo);
			add_filter("manage_edit-factory_columns",		 		array($this, 	'factory_add_views_column'), 5);
			add_filter('manage_factory_posts_custom_column', 		array($this,	'factory_fill_views_column'), 6, 2); 
			add_filter('manage_edit-smp_hub_columns',		 				array($this, 	'smp_hub_add_views_column'), 5);
			add_filter('manage_smp_hub_posts_custom_column', 				array($this,	'smp_hub_fill_views_column'), 6, 2); 
			
			add_filter( 'plugin_action_links', 								array($this, 'plugin_action_links'), 10, 2 );
			add_action( 'wp_before_admin_bar_render', 						array(SMCO_Assistant, 'my_admin_bar_render' ), 14);
			
			if($this->options['supported_types']['user'])
			{
				add_action( 'show_user_profile', 							array($this, 'extra_user_profile_fields' ));
				add_action( 'edit_user_profile', 							array($this, 'extra_user_profile_fields' ));
				add_action( 'edit_user_profile_update', 					array($this, 'edit_user_profile_update' ),11);
				add_action( 'personal_options_update', 						array($this, 'edit_user_profile_update' ),11);
				add_action( 'manage_users_columns',							array($this, 'user_column' ));
				add_filter( 'manage_users_custom_column', 					array($this, 'show_user_columns'), 10, 3 );					
			}
			if($this->options['supported_types'][SMC_LOCATION_NAME])
			{
				add_action( SMC_LOCATION_NAME.'_edit_form_fields', 			array($this, 'edit_location_type_to_location'), 9, 2 );
				add_filter("smc_".SMC_LOCATION_NAME."_tab_over_params",		array($this, 'smc_location_tab_over_params'), 90, 3);
				add_action("add_".SMC_LOCATION_NAME."_tab",					array($this, 'add_location_tab'), 10, 2);
				add_filter("location_display_tbl1", 						array(SMCO_Assistant, "location_display_tbl1"), 12, 4);
			}
			add_action("smc_wll",									array($this, "foo_smc_login_list"), 8);
			add_action("smc_login_form_picto",						array($this, "smc_login_form_picto1"), 12); 
			
			add_filter("smp_butch_slide_4",							array($this, "smp_butch_slide_4"), 10, 2);
			add_filter("smp_batch_card_price",						array($this, "smp_batch_card_price"), 10, 2);
			add_action("smp_send_batch_changes",					array($this, "smp_send_batch_changes"), 10, 1);
			add_action("smp_rize_batch",							array($this, "smp_rize_batch"));
			
			add_action("smp_admin_factory_box_func",				array($this, "smp_admin_factory_box_func"));
			add_action("smp_admin_factory_save_box_data",			array($this, "smp_admin_factory_save_box_data"), 10, 2);
			add_action("smp_admin_smp_hub_box_func",				array($this, "smp_admin_smp_hub_box_func"));
			add_action("smp_admin_smp_hub_save_box_data",			array($this, "smp_admin_smp_hub_save_box_data"), 10, 2);
			//choose special pages in metagame panel
			add_filter("smc_admin_special_pages_choosee", 			array($this, 'smc_admin_special_pages_choosee'));
			add_action("smc_admin_sinc_special_pages",				array($this, 'smc_admin_sinc_special_pages'));
			
			add_filter( 'smc_load_panel_screen',					array($this, 'smc_load_panel_screen'), 12, 2 );
			add_filter( 'smp_calc_personal_needs',					array($this, 'smp_calc_personal_needs'), 1 );
			add_filter( 'smp_calc_location_needs',					array($this, 'smp_calc_location_needs'), 1, 2 );
			add_filter( 'smp_calc_factory_needs',					array($this, 'smp_calc_factory_needs'), 1, 2 );
			add_filter("open_location_content_widget",				array($this, "open_location_content_widget"), 12, 2);
			
			add_action( 'admin_notices',							array($this, 'after_install_sticker') , 45);
			//add_filter( 'smc_display_special_panel',array($this, 'smc_display_special_panel'), 10, 1 );
			
			add_filter("smco_consumed_by_industry",					array($this, "smco_consumed_by_industry"), 10, 5);
			
			add_filter( 'cron_schedules', 							array($this, 'cron_add_interval'),10 );
			if ( ! wp_next_scheduled( 'consume_hook' ) ) 
			{
			  wp_schedule_event( time(), 'consume_interval', 'consume_hook' );
			}
			add_action( 'consume_hook', 						array($this, 'production_handler' ));
			
			add_action( 'smc_add_option',						array(SMCO_Assistant, 'smc_add_option') , 12);
			add_action( 'smc_add_object_type',					array(SMCO_Assistant, 'smc_add_object_type') , 12);
			add_filter("smp_my_tools",							array(SMCO_Assistant, 'smp_my_tools'), 9);		
			add_filter("ermak_booker_mobile_admin",				array(SMCO_Assistant, 'ermak_booker_mobile_admin'), 9);		
			add_action("ermak_booker_mobile_admin_save",		array(SMCO_Assistant, 'ermak_booker_mobile_admin_save'), 9);	
			add_filter("ermak_booker_mobile_slide",				array(SMCO_Assistant, 'ermak_booker_mobile_slide'), 9, 3);			
			SMCO_Report::init();
		}
		
		
		
		function cron_add_interval( $schedules ) 
		{
			$schedules['consume_interval'] = array(
				'interval'		=> $this->options['consume_interval'] * 60,
				'display'		=> __( 'consume_interval', "smco" ),
			);
			return $schedules;
		}
		function production_handler($breaking=false)
		{
			if(!$this->options['consumes_started'] && !$breaking)		return;
			$current_circle_id		= (int)get_option("current_consume_circle_id");
			do_action("before_smco_count_cirle", $current_circle_id);
			//insertLog("SCMOclass.php production_handler", "finished $current_circle_id consume circle.");
			$this->smp_count_circle($current_circle_id);
			do_action("after_smco_count_cirle", $current_circle_id);
			$current_circle_id++;
			update_option("current_consume_circle_id", $current_circle_id);
			//insertLog("SCMOclass.php production_handler", "started $current_circle_id consume circle.");
		}
		
		
		function after_install_sticker()
		{
			$install_ermak_consume 		= get_option('install_ermak_consume');
			if(!($install_ermak_consume == 1))	return;
			echo "
			<div class='updated notice smc_notice_ is-dismissible1' id='install_ermak_dragon_notice' >
				<p>".
					$this->get_sticker_button().
				"</p>
				<span class='smc_desmiss_button' setting_data='install_ermak_consume'>
					<span class='screen-reader-text'>".__("Close")."</span>
				</span>
			</div>
			
			";
		}
		function get_sticker_button()
		{
			return "
			<div class='smc_notice'>
				<h2>".
				__("Ermak Consume", "smco").
				"</h2>
				<h4>".
				__("Select one of packeges schemes for install to your project.", "smco").
				"</h4>
				<div>".
					__("Note: Check if you have the appropriate packages 'Industry' for the desired type of consumption.", "smco").
				"</div>
			</div>
			<div class='smc_notice smco_traditional' style=''>
				<div class='smc_notice_cube2 ' smco_inst='traditional'>".
					__('traditional consume','smco').
				"</div>
			</div>
			<div class='smc_notice smco_fantasy' style=''>
				<div class='smc_notice_cube2 ' smco_inst='fantasy'>".
					__('fantasy consume','smco').
				"</div>
			</div>
			<div class='smc_notice smco_politeconomics_scheme' style=''>
				<div class='smc_notice_cube2 ' smco_inst='modern'>".
					__('modern consume','smco').
				"</div>
			</div>";
		}
		
		function plugin_action_links( $links, $file )
		{
			if ( $file == 'Ermak_consume/Ermak-consume.php'  ) 
			{
				$links[] = '<a href="' . admin_url( 'admin.php?page=smco_consume_settings_menu_sub_menu' ) . '">'. __("Settings") .'</a>';
			}

			return $links;
		}
		
		function admin_page_handler()
		{
			add_menu_page( 
						__('Ermak. Consuming', "smco"), // title
						__('Ermak. Consuming', "smco"), // name in menu
						'manage_options', // capabilities
						'Metagame_Consuming_page', // slug
						array($this, 'consume_setting_pages'), // options function name
						SMCo_URLPATH .'img/ermak_market_logo.png', // icon url 'none', 
						'20.601'
						);
		}
		function admin_subpage_handler()
		{
			$col		= (int)get_option("smco_col_arrears");
			add_submenu_page( 
							'Metagame_Consuming_page'  //or 'options.php' 
							, __('Arrears', 'smco')					
							, __('Arrears', 'smco')	. "<span class='awaiting-mod smco_arrears_count'><span class='smco_arrears_count_number'>".$col."</span></span>"	
							, 'manage_options'
							, 'smco_consume_settings_menu_sub_menu'
							, array($this,'menu_consuming_page')
						);		
		}
		function menu_consuming_page()
		{		
			require_once('SMCO_MainPult.php');
			echo '<div style="display:inline-block; margin-right:15px;">'.SMCO_MainPult::get_admin_form().'</div>';
		}
		
		/*
			@post_type_name - 'user', 'location', SMP_FACTORY, 'smp_hub' ets.
		*/
		static function is_type_supported($post_type_name)
		{
			global $Ermak_Consume;
			return $Ermak_Consume->options['supported_types'][$post_type_name];
		}
		function consume_setting_pages()
		{
			global $wpdb;
			$post_types		= get_post_types('','object');
			$taxonomies		= get_taxonomies(array(), "object");
			
			require_once(SMC_REAL_PATH.'class/SMC_Object_type.php');
			$smc_object		= SMC_Object_Type::get_instance();
			//echo Assistants::echo_me($smc_object, true);
			//server side
			if(current_user_can("administrator"))
			{
				$current_circle_id		= (int)get_option("current_consume_circle_id");
				if($_POST['check_all_settings'])
				{
					if($_POST['create_delete1']==1)
					{
						SMCO_Assistant::restore_data();
					}
				}
				if($_POST['reset_all_settings'])
				{
					if($_POST['create_delete']==1)
					{
						//SMCO_Assistant::restore_data();
						echo "RESET ALL";
					}
				}
				if($_POST['unnull_all_raitings'])
				{
					if($_POST['status'] == 'on')
					{
						SMCO_Assistant::restore_all_statuses();
					}
				}
				if($_POST['circler'])
				{
					
					$this->production_handler(true);
				}
				if($_POST['set_settings'])
				{
					if(!isset($this->options['supported_type_types']))
						$this->options['supported_type_types'] 	= array();
					if(!isset($this->options['supported_type_names']))
						$this->options['supported_type_names'] 	= array();
					if($_POST['user']=='on' )
					{
						$this->options['supported_types']['user'] 		= 1;
						$this->options['supported_type_types']['user'] 	= 'user';
						$this->options['supported_type_names']['user'] 	= __("Player", "smc");
					}
					else
					{
						$this->options['supported_types']['user'] 		= 0;
					}
					foreach ($post_types as $post_type ) 
					{
						if(
							$post_type->name == 'nav_menu_item' ||
							$post_type->name == 'revision' ||
							$post_type->name == 'post' ||
							$post_type->name == 'attachment' ||
							$post_type->name == 'goods_type' ||
							$post_type->name == 'smc_currency_type' ||
							$post_type->name == 'location_type' ||
							$post_type->name == SMCO_CONSUME_SCHEME_TYPE
						  )
							continue;
						$this->options['supported_types'][$post_type->name] 		= $_POST[$post_type->name]=='on' ? 1 : 0;
						$this->options['supported_type_types'][$post_type->name] 	= 'post';
						$this->options['supported_type_names'][$post_type->name] 	= $post_type->label; 
						//echo $post_type->name . ":" . $_POST[$post_type->name]."<BR>";
					}
					
					foreach ($taxonomies as $taxonomy ) 
					{
						if(
							$taxonomy->name == 'nav_menu' ||
							$taxonomy->name == 'post_tag' ||
							$taxonomy->name == 'link_category' ||
							$taxonomy->name == 'post_format'
						  )
							continue;
						$this->options['supported_types'][$taxonomy->name] 			= $_POST[$taxonomy->name]=='on' ? 1 : 0;
						$this->options['supported_type_types'][$taxonomy->name] 	= 'taxonomy';
						$this->options['supported_type_names'][$taxonomy->name] 	= $taxonomy->label; 
					}
					
				}
				if($_POST['doing'])
				{
					echo "report<BR>";
					//var_dump( SMCO_Assistant::get_location_charges_id(209));
					var_dump( SMCO_Assistant::get_all_post_types());
				}
				if($_POST['start_all_consumes'])
				{
					$this->options['consumes_started']		= true;
					echo 'start';
				}
				if($_POST['finish_all_consumes'])
				{
					$this->options['consumes_started']		= false;
					echo 'finish';
				}
				if($_POST['saved'])
				{
					$this->options['consume_interval']		= $_POST['consume_interval'];
				}
				if($_POST["update_circle_id"])
				{
					$ccid				= $_POST['current_consume_circle_id'];
					$query				= "DELETE FROM `". $wpdb->prefix.REPORT_DB_PREFIX . "` WHERE report_id>" . $ccid . " AND report_type=" . CONSUME_REPORT_TYPE;
					echo $query;
					$wpdb->get_results($query);
					update_option("current_consume_circle_id", $_POST['current_consume_circle_id']);
					$current_circle_id		= (int)get_option("current_consume_circle_id");
				}
			}
			update_option(SMCo, $this->options);
			
			//echo Assistants::echo_me($this->options, true);
			
			echo "<h2>". __('Metagame Consuming Settings', "smco") . "</h2>
			<form name='settings'  method='post'  enctype='multipart/form-data' id='settings'>";
			
			
			$main		= '
			<div class="smc_form" style="display: inline-block; ">
				<div class="h" style=" background:#1F2F54; color:white; padding: 10px; max-width:100%;">
					<board_title>'.sprintf(__("Now %s consume circle is during", "smco"), "<input type='number' min='0' name='current_consume_circle_id' class='smc_number' value='".$current_circle_id."'/>").
					' <div class="smc_bottom" style="" name="update_circle_id"><i class="fa fa-check"></i></div></board_title>
				</div>
				<div class="h0">					
					<div class="h" style="display:'.($this->options['consumes_started'] ? "none" : "block") .'; background:#111; color:white;">
						<board_title>'.__("Start Live","smco").'</board_title>
						<input type="submit" name="start_all_consumes" class="smp-button" value="'.__("Start Live","smco").'"/>
					</div>
					
					<div class="h"  style="display:'.($this->options['consumes_started'] ? "block" : "none") .'; background:#111; color:white;">
						<board_title>'.__("Paused Live","smco").'</board_title>
						<input type="submit" name="finish_all_consumes" class="smp-button" value="'.__("Paused Live","smco").'" style="background:red;"/>
					</div>
					
					<div class="h"  style=" color:white;background:#8CB835;">
						<board_title>'.__("Set Consume Inteval","smco").'</board_title>
						<input  type="number" step="1" min="1" name="consume_interval" class="smc_number" value="'. $this->options["consume_interval"] .'"/>
						<label>'.__(" minutes", "smp").'</label>	
						<div class="smc_bottom" style="" name="saved"><i class="fa fa-check"></i></div>
						<!--input type="submit" name="saved" value="' .__("Save").'"/-->			
					</div>
					
					<div class="h" style=" background:#1F2F54; color:white;">
						<input type="submit" class="smc_bottom_width" style="" name="circler" value="'.__("update circle, please", "smco").'"/>
					</div>
				</div>
				<div class="h0">
					<div  class="h" style="padding:15px; color:white; background:#777;width:300px;">
						<board_title>'.__("Supported Types", "smco") . '</board_title>
							<input type="checkbox" class="css-checkbox" name="user" id="user"'. checked($this->options["supported_types"]["user"], true, false) . '/>						
							<label class="css-label" for="user">'. __("Player", "smc") . '</label>
							<br>';
							foreach ($post_types as $post_type ) 
							{
								$nm			= $smc_object->get($post_type->name);
								//$main		.= Assistants::echo_me($smc_object->get($post_type->name)['owner_id'], true);
								if(!isset( $nm['owner_id'])) continue;
								$main		.= '
								<input type="checkbox" class="css-checkbox" name="'.$post_type->name.'" id="'.$post_type->name.'" '. checked( $this->options['supported_types'][$post_type->name], true, false ).' />						
								<label class="css-label" for="'.$post_type->name.'">'. $post_type->label. '</label>
								<br>';
							}
							
							foreach ($taxonomies as $taxonomy ) 
							{
								$tx			= $smc_object->get($taxonomy->name);
								if(!isset( $tx['owner1'])) continue;
								//echo '<br>'. $taxonomy->label;
								$main		.= '
								<input type="checkbox" class="css-checkbox" name="'.$taxonomy->name.'" id="'.$taxonomy->name.'" '. checked($this->options['supported_types'][$taxonomy->name], true, false).' />						
								<label class="css-label" for="'.$taxonomy->name.'">'. $taxonomy->label. '</label>
								<br>';
							}
							$options								= get_option(SMCo);
						
			$main		.= '
						<p style="margin-top:10px;"></p>
						<input type="submit" class="smc_bottom_width" name="set_settings" value="'.__("Save"). '" />
					</div>			
				</div>			
			</div>';
			
			$manipul =  '
			<div class="smc_form" style="display: inline-block; ">
			<div style="display:inline-block; position:relative; float:left; color:white;">
				<div style="padding:15px; background:#8CB835;">
					<board_title>'. __("Restore the integrity of connections", "smco") . '</board_title>
					<p style="font-style:italic;">'.
						__("Check availability data with all supported types. Recreate all need data and object for successfull functionslity.", "smco").
					'</p>
					<input type="radio" name="create_delete1" id="is_locked11" value=0 checked="checked" />
					<label for="is_locked11" class="">' . __("Not do it!", "smco") . '</label> 
					<p>
					</p>
					<input type="radio" name="create_delete1" id="is_locked12" value=1 />
					<label for="is_locked12" class="">'  . __("Do it!", "smco") . '</label>
					<p style="margin-top:10px;">
					</p>
					<input type="submit" class="smc_bottom_width"  value="' .__("Start proccess of check and recreating", "smco"). '" name="check_all_settings"/>
				</div>
				
				<div style="padding:15px; background:#941D10;">
					<board_title>'. __("Reset All Settings", "smco"). '</board_title>
					<p style="font-style:italic;">'.
						__("Delete all data for all supported types", "smco").
					'</p>
					<input type="radio" name="create_delete" id="is_locked1" value=0 checked="checked" />
					<label for="is_locked1" class="">'. __("Not do it!", "smco"). '</label> 
					<p>
					</p>
					<input type="radio" name="create_delete" id="is_locked2" value=1 />
					<label for="is_locked2" class="">'.__("Do it!", "smco").'</label>
					<p style="margin-top:10px;">
					</p>
					<input type="submit"  class="smc_bottom_width" value="' . __("Start of deleting proccess", "smco") . '" name="reset_all_settings"/>
				</div>
				
				<div style="padding:15px; background:#516130;">
					<board_title>'. __("Restore Settings for Consumers to presets", "smco") . '</board_title>
					<p style="font-style:italic;">'.
						__("Select one or more parameters to restoring for all Consumers.", "smco").
					'</p>	
					<div style="margin:10px 0; ">
						<p>
							<input type="checkbox" class="css-checkbox" name="status" id="status"/>
							<label for="status" class="css-label">'.__("all statuses to 100%", "smco") . '</label>
						</p>
					</div>
					<input type="submit" class="smc_bottom_width"  value="' . __("Restore").'" name="unnull_all_raitings"/>
				</div>
				<div style="padding:15px; background:#1F2F54;">
					<input type="submit" class="smc_bottom_width"  name="doing" value="i know what i doing!"/>
				</div>
			</div>
			</div>';
			
			//$install_ermak_consume 		= get_option('install_ermak_consume');
			//if($install_ermak_consume != 1)
			{
				$presets		.= '<div class="smc_form" style="display: inline-block; ">
				<div style="padding:15px;  width:390px; background:#AAA;">'.
					$this->get_sticker_button().
				'</div>
				</div>';
			} 			
			
			
			$map				= SMCO_Assistant::get_cs_map();
			echo Assistants::get_switcher( 
												array(
														array("title" => __("Settings"), 			"slide" => $main ),			
														//array("title" => __("Map", 'smc'), 		"slide" => $map),		
														array("title" => __("Actions"), 			"slide" => $manipul),				
														array("title" => __("Presets","smco"), 		"slide" => $presets),	
														apply_filters("smco_setting_page", null)														
													 )
											 );
			//var_dump($this->options);
			?>	
				</form>
			<?php
		}
		
		static function consume_status_picto($type="user", $id, $width=100, $height=22, $params="")
		{
			global $Ermak_Consume;
			//return $type.": ".$Ermak_Consume->get_consume_status($type, $id);
			return SMP_Assistants::get_quality_diagramm($Ermak_Consume->get_consume_status($type, $id), $width, $height);
		}
		
		
		function smp_admin_factory_box_func($factory_id)
		{
			if(self::is_type_supported(SMP_FACTORY))
			{
				?>
				<div class='h'>
					<label for="consume"><?php _e("Consume Scheme", "smco"); ?></label><BR>
					<?php $consume	= get_post_meta($factory_id, 'consume_scheme_id', true); 
					echo SMCO_Assistant::wp_drp_all_consumer_schemes( array('selected'=>$consume, 'type'=>SMP_FACTORY,  'name'=>"consume_scheme_id",'id'=>"consume", 'style'=>'width:25em;')); 
					?>
				</div>
				<?php 
			}
		}
		function smp_admin_factory_save_box_data($post_id, $POST)
		{
			update_post_meta($post_id, 'consume_scheme_id', 			$POST['consume_scheme_id']);
		}
		function factory_add_views_column( $columns )
		{
			if(self::is_type_supported(SMP_FACTORY))
				$columns["consume"]		= __("Consume", 'smco');
			return $columns;			
		}
		function factory_fill_views_column($column_name, $post_id)
		{
			switch( $column_name) 
			{
				case 'consume':
					$consume	= get_post_meta($post_id, 'consume_scheme_id', 1);
					$con		= $consume == "" ? "" : $this->admin_consume_form($consume, true);				
					echo $con;
					break;
			}
		}
		
		//
		function smp_admin_smp_hub_box_func($factory_id)
		{
			?>
			<div class='h'>
				<label for="consume"><?php _e("Consume Scheme", "smco"); ?></label><BR>
				<?php $consume	= get_post_meta($factory_id, 'consume_scheme_id', true); 
				echo SMCO_Assistant::wp_drp_all_consumer_schemes( array('selected'=>$consume, 'type'=>'smp_hub',  'name'=>"consume_scheme_id",'id'=>"consume", 'style'=>'width:25em;')); 
				?>
			</div>
			<?php
		}
		function smp_admin_smp_hub_save_box_data($post_id, $POST)
		{
			update_post_meta($post_id, 'consume_scheme_id', 			$POST['consume_scheme_id']);
		}
		function smp_hub_add_views_column( $columns )
		{
			$columns["consume"]		= __("Consume", 'smco');
			return $columns;			
		}
		// заполняем колонку данными	
		function smp_hub_fill_views_column($column_name, $post_id)
		{
			switch( $column_name) 
			{
				case 'consume':
					$consume	= get_post_meta($post_id, 'consume_scheme_id', 1);
					$con		= $this->admin_consume_form($consume, true);				
					echo $con;
					break;
				default:
					null;
			}
		}
		
		
		
		function extra_user_profile_fields($user)
		{
			?>
				<table class="form-table">
					<tr>
					<th><label for="consume"><?php _e("Consume", "smco"); ?></label></th>
						<td>
							<?php $consume	= get_user_meta($user->ID, 'consume'); 
							$consume		= $consume[0];
							echo SMCO_Assistant::wp_drp_all_consumer_schemes( array('selected'=>$consume, 'type'=>'user', 'name'=>"consume",'id'=>"consume", 'style'=>'width:25em;')); ?>
						
						</td>
					</tr>
				</table>
			<?php
		}
		function edit_user_profile_update($user_id)
		{
			if ( current_user_can('edit_user',$user_id) )
			 {
				 update_user_meta($user_id, 'consume', $_POST['consume']);				
			 }
		}
		function user_column($columns)
		{
			$columns['consume']				= __("Consume", "smco");
			return $columns;
		}
		function show_user_columns($value, $column_name, $user_id)
		{
			//return $value;
			switch($column_name)
			{
				case "consume":
					$consume	= get_user_meta($user_id, 'consume'); 
					$consume	= $consume[0];
					$cs			= get_post($consume);
					
					$con		= $this->admin_consume_form($consume, true);					
					
					return "<a href='/wp-admin/post.php?post=" . $consume . "&action=edit'>" . $cs->post_title."</a><br>" . $con . self::consume_status_picto("user", $user_id);
					break;						
			}
		}
		public function get_user_consume_id($cont_id, $consumer_type, $id)
		{
			switch($consumer_type)
			{
				case SMC_LOCATION_NAME:
					$data		= SMC_Location::get_term_meta($cont_id);//get_option("taxonomy_$user_id");					
					$consume	= $data['consume'];
					break;
				case "user":
					$consume	= get_user_meta($cont_id, 'consume', true); 
					break;
				case "smc_post":
				case "smp_hub":
				case SMP_FACTORY:
					$consume	= get_post_meta($id, 'consume_scheme_id', true); 
					break;
				default:
					$consume	= apply_filters("scmo_get_consume_id", $consumer_type, $cont_id, $id);
			}
			//$consume	= $consume[0];
			return $consume;
		}
		public function get_consume_goods($consume_scheme_id)
		{
			$goods		= get_post_meta($consume_scheme_id, "goods", true);
			$goods_arr	= array();
			$i=0;
			if($goods != "")
			{
				if(count($goods)>0)
				{
					foreach($goods as $gd)
					{
						$gg							= $gd[0]['goods_type'];
						if(!isset($goods_arr[$gg]))	$goods_arr[$gg] = array();
						$goods_arr[$gg]				=  array("goods_value"=>$gd[0]["goods_value"], "payments_summ"=>$gd[0]["payments_summ"]);
						$i++;
					}
				}
			}
			return $goods_arr;
		}
		public function get_consume_scheme($consume_scheme_id)
		{
			return get_post($consume_scheme_id);
		}
		public function get_consume_scheme_meta($consume_scheme_id, $meta_name)
		{
			return get_post_meta($consume_scheme_id, $meta_name, true);
		}
		public function get_consume_data($consume_scheme_id, $count=1)
		{
			global $SolingMetagameProduction;
			$data		= array();
			$sceme		= $this->get_consume_scheme($consume_scheme_id);
			if(!$sceme)	return array();
			$goods		= $this->get_consume_scheme_meta($consume_scheme_id, "goods");
			$choose		= $this->get_consume_scheme_meta($consume_scheme_id, "choose");
			if($goods != "")
				if(count($goods)>0)
				{
					foreach($goods as $gd)
					{
						$val		= $gd[0]["goods_value"] * $count;
						if(!$val)	continue;
						$goods_id	= $gd[0]['goods_type'];
						$summ		= $gd[0]['payments_summ']*$val;
						$data[$goods_id]	= array(
							"need"	=> $val,
							"money"	=> $summ
						);
					}
				}
			return $data;
		}
		public function admin_consume_form($consume_scheme_id, $rounded=false, $count=1 , $is_title=true, $currency_type_id=-1)
		{
			if(!$consume_scheme_id)	return "<p class='description center'>".__("No scheme exists for this consumer", "smco")."</p>";
			global $SolingMetagameProduction;
			$sceme		= $this->get_consume_scheme($consume_scheme_id);
			
			$con		=  "<div style='display:block; width:100%;'>";
			$con		.=  $is_title ? "<div>" . $sceme->post_title . "</div>" : "";
			$goods		= $this->get_consume_scheme_meta($consume_scheme_id, "goods");
			$choose		= $this->get_consume_scheme_meta($consume_scheme_id, "choose");
			$goods_arr	= array();
			//var_dump($goods);
			$i=0;
			if(is_array($goods))
			{
				if(count($goods)>0)
				{
					foreach($goods as $gd)
					{
						$val		= $gd[0]["goods_value"] * $count;
						if(!$val)	continue;
						$goods_id	= $gd[0]['goods_type'];
						if($gd[0]['payments_summ']>0 && $SolingMetagameProduction->is_finance())
						{
							$summ		= $gd[0]['payments_summ']*$val;
							if($rounded) $summ = round($summ);
							if($currency_type_id!=-1)
							{
								$ct			= SMP_Currency_Type::get_instance($currency_type_id);
								$summ_text	= $ct ? $ct->get_price($summ, "large_doubled", $rounded) : $summ . __("UE", "smp");
							}
							else
							{
								$summ_text	= "<st class='large_doubled'>" . ($summ) . "</st> " . __("UE", "smp");
							}
							$payments	= "<span class='admin_form_payments hint hint--top' data-hint='".__("Autopay summ", "smco")."'>" . $summ_text . "</span>";
							
						}
						else
							$payments	= "";
						$class		= ($i%2==0) ? "smco_table_li_0": "smco_table_li_1";
						$con		.= "<div class='" . $class . "'>";
						//$goods 		= get_post($goods_id);
						$ind		= SMP_Industry::get_instance($goods_id);
						$title		= $ind->get("resourse_name");
						if($rounded)	$val 	= round ($val);
						$con		.= "<div>".
							"<span style=' width: 30%;position: relative;display: inline-block;'>".$title . "</span>".
							$payments.
							"<span class='admin_form_gvalue'>" . ($val) . "</span><span style='position:absolute; right:4px;'> ".__("unit", "smp")."</span>
							
							</div>";
						$con	.= '</div>';
						$i++;
					}
				}
				else $con.= "<div class='smc-description'>".__("Scheme is empty", "smco")."</div>";
			}
			else
			{
				$con.= "<div class='smc-description'>".__("Scheme is empty", "smco")."</div>";
			}
			$con	.= '</div>';
			return $con;
		}
		
		//Consume Scheme Post Type
		
		
		function add_cs_type()
		{
			
			global $smc_main_tor_buttons;
			if(!isset($smc_main_tor_buttons))		$smc_main_tor_buttons = array();
			$smc_main_tor_buttons[]					= array('ico'=>"", 'targ'=>__('Consume', "smco"), 'comm'=>'get_current_location_consume');
			$labels 	= array
			(
				'name' => __('Consume Scheme', "smco"),
				'singular_name' => __("Consume Scheme", "smco"), // админ панель Добавить->Функцию
				'add_new' => __("add Consume Scheme", "smco"),
				'add_new_item' => __("add Consume Scheme", "smco"), // заголовок тега <title>
				'edit_item' => __("edit Consume Scheme", "smco"),
				'new_item' => __("add Consume Scheme", "smco"),
				'all_items' => __("all Consume Schemes", "smco"),
				'view_item' => __("view Consume Scheme", "smco"),
				'search_items' => __("search Consume Scheme", "smco"),
				'not_found' =>  __("Consume Scheme not found", "smco"),
				'not_found_in_trash' => __("no found Consume Scheme in trash", "smco"),
				'menu_name' => __("Consume Scheme", "smco") // ссылка в меню в админке
			);
			$args 		= array
			(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true, // показывать интерфейс в админке
				'has_archive' => true, 
				'exclude_from_search' => true,
				'menu_position' => 5, // порядок в меню
				'show_in_menu' => "Metagame_Consuming_page",
				'supports' => array(  'title' )
				//,'capabilities' => 'manage_options'
				,'capability_type' => 'post'
			);
			register_post_type(SMCO_CONSUME_SCHEME_TYPE, $args);
		}
		
		// мета-поля в редактор		
		function my_cs_extra_fields() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(&$this, 'extra_cs_fields_box_func'), SMCO_CONSUME_SCHEME_TYPE, 'normal', 'high'  );
		}
		
		function extra_cs_fields_box_func( $post )
		{		
			global $Soling_Metagame_Constructor;
			$meta_val		= get_post_meta($post->ID, "supported_type_name", true);
			
			?>
			<div style='display:inline-block;'>
				<div style='float:left; position:relative; display:inline-block;'> 
					<div class='h5 h'>	 					
						<label  class="h5" for="supported_type_name"><?php echo __("Called Type Name", "smco"); ?></label><br>
						<select  class="chosen-select" name="supported_type_name" id="supported_type_name">
							<option value="-1">---</option>
							<?php
								
								foreach($this->options['supported_types'] as $typ=>$val)
								{
									if($val == 1)
									{
										//$post_type			= get_post_type
										echo "<option value='".$typ."' ".selected($typ, $meta_val, false).">".$this->options['supported_type_names'][$typ]."</option>";
										
									}
								}
							?>
						</select>
					</div>			
					<div class='h h5'>	
						<label  class="h5" for="supported_type_name"><?php echo __("Goods", "smco"); ?></label>
						
						<div style="vertical-align:middle;" id="scheme">							
						<?php echo SMP_Industry::wp_numbering_list(array("name"=>"goods_type_cs0", "payments"=>true), get_post_meta($post->ID, 'goods', true)); ?>						
						</div>
					</div>		
					<!--div class='h h5'>	
						<label  class="h5" for="prevous"><?php echo __("Prevous Consume Scheme", "scmo"); ?></label>						
						<div style="vertical-align:middle;">							
							<?php echo SMCO_Assistant::wp_drp_all_consumer_schemes( array('class'=>'chosen-select', "name"=>"prevous", "exclude"=> array($post->ID), 'selected' =>get_post_meta($post->ID, 'prevous', true) ) ); ?>						
						</div>
					</div-->	
					
				</div>	
							
			</div>
			<script type="text/javascript">
				set_chosen(".chosen-select", {max_selected_options: 5});
				jQuery(function($)
				{					
					$(".rangerr").ionRangeSlider({
							min: -100,
							max: 100, 			
							type: 'single',
							step: 5,
							postfix: "%",
							prettify: true,
							hasGrid: true
					});
				});
			</script>
			<?php 
			wp_nonce_field( basename( __FILE__ ), 'consume_scheme_metabox_nonce' );
		}
		
		function true_cs_save_box_data ( $post_id) 
		{	
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['consume_scheme_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['consume_scheme_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			/**/
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;				
			update_post_meta($post_id, 'supported_type_name',	$_POST['supported_type_name']);	
			update_post_meta($post_id, 'prevous',				$_POST['prevous']);	
			$goods							= array();
			for($i=0; $i<(int)$_POST['count_industries']; $i++)
			{
				$goods_t					= $_POST['ids_' . $i];
				$goods_val					= $_POST['cnt_' . $i];
				$payments_summ				= $_POST['cnt_autopay_' . $i];
				$goods[]					= array( array( "goods_type" => $goods_t, "goods_value" => $goods_val, "payments_summ"=> $payments_summ ) );
			}
			update_post_meta($post_id, 'goods', 				 $goods);	
			return $post_id;
		}
		
		
		function add_cs_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 					=> " ",
				  "IDs"	 				=> __("ID", 'smco'),
				  "title"	 			=> __("Title"),
				  "supported_type_name"	=> __("Supported Types", 'smco'),
				  "prevous"				=> __("Prevous", 'smco'),
				  "goods" 				=> __("Goods", "smp"),
			   );
			return $posts_columns;			
		}
		// добавляем возможность сортировать колонку
		function add_cs_views_sortable_column($sortable_columns){
			$sortable_columns['IDs'] 					= 'IDs';						
			$sortable_columns['supported_type_name'] 	= 'supported_type_name';				
			$sortable_columns['goods'] 					= 'goods';				
			$sortable_columns['prevous'] 				= 'prevous';				
			return $sortable_columns;
		}	
		// заполняем колонку данными	
		function fill_cs_views_column($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{		
				case 'IDs':
					echo $post_id;
					break;		
				case 'supported_type_name':
					echo $this->options['supported_type_names'][get_post_meta($post_id, 'supported_type_name', true)];
					break;			
				case 'prevous':
					$prev_id		= get_post_meta($post_id, 'prevous', true);
					$prev			= get_post($prev_id);
					echo $prev_id 	!= '' ? $prev->post_title : '--';
					break;			
				case 'goods':
					echo $this->admin_consume_form($post_id, true);
					break;	
			}		
		}
		
		
		
		// SMC FILTERS
		function smc_load_panel_screen($html, $currenting_url)
		{
			global $post;
			global $Soling_Metagame_Constructor;
			$div0	= "<div class=lp-wrap-open style=width:100%; height:100%; font-size:16px; font-family:Ubuntu Condensed; color:white; padding:50px;>";
			$div1	= '</div>';
			switch($post->post_type)
			{
				case "smco_consume_scheme":	
					$trum_img	= SMCo_URLPATH."img/consume.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("Consume Scheme", "smco") . "</div><span class=lp_title>".$post->post_title."</span></center></div>')";
			}	
			
			$prfx		= get_bloginfo("url"). "/?page_id=";
			switch($currenting_url)
			{
				case $prfx . $this->options['user_consumes_page_ID']:
					$trum_img	= SMCo_URLPATH."img/consume.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title style=width:300px;><div class=smc_special_title>" . 
					__("My Consumes", "smco") . "</div><div class=smp-comment_1>" . sprintf(__("You must eat and dress every Playing Circle. For successfull consuming you must add to your Consuming Bag (this page) a certain number of goods and money.<BR>Make necessary manipulations in your My goods page and Bank page.<BR>System writed-off the appropriate number of goods and money in moment of start next Playing Circle. You will be sorry if operation will be unsuccessful.", "smco"), 
					"<a href=#><b>ff</b></a>") . 
					"</div></center><div class=lp_margin_left>".
					"</div></div>')";
			}
			return $html;
		}
		
		function smc_display_special_panel($src)
		{
			
			$html 	= array(
								'text'=> $src , 
								'exec'=> '', //'open_metagame_panel', 
								'args'=> '', //array('special_filtered_panel', $src),
							);
			return $html;
		}
		
		
		
		// SMC Login Widget Element
		function foo_smc_login_list()
		{
			?>
				<span><i class="fa fa-angle-right"></i></span> <a href="<?php echo get_permalink($this->options['user_consumes_page_ID']); ?>"> <?php _e("My Consumes", "smco"); ?></a><?php echo self::consume_status_picto('user', get_current_user_id(), 60)?></br> 				
			<?php
		}
		function smc_login_form_picto1()
		{
			?>
				<div class="hint hint--top page_icon" data-hint="<?php _e("My Consumes", "smco"); ?>" href="<?php echo get_permalink($this->options['user_consumes_page_ID']); ?>">
					<img src="<?php echo SMCo_URLPATH . "img/consuming_ico.png"?>"> 					
				</div>
			<?php
		}
		
		static function  is_smc_install()
		{
			return is_plugin_active('Ermak/Ermak.php');
		}
		static function is_smp_install()
		{
			return is_plugin_active('Ermak_production/Ermak_production.php');
		}
		static function is_finance()
		{
			if(!self::is_smp_install())	return false;
			global $SMP_Currency;
			return $SMP_Currency->options['finance_enabled'];			
		}
		
		static function install()
		{
			if(self::is_smc_install())
			{
				SMC_Location::add_properties(array("consume", "user_consume_status", "last_user_consume_status"), "INT UNSIGNED ", 0);
			}	
			$my_post = array(
								  'post_title'   		=> __("My Consumes", "smco"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smco_user_consume]"  ,
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);

			return wp_insert_post( $my_post ); 
		}
		
		
		// Goods Batch
		
		function smp_batch_extra_fields($args,   $post_id )
		{
			
			?>
				<div class="h h2">
					<label for="consumer_id"><?php echo __("Consumer", "smco");?></label><br>					
					<?php
						echo get_post_meta($post_id, "consumer_id", true)."<BR>";
						//echo SMCO_Assistant::wp_users_consumers(array('selected' => get_post_meta($post_id, "consumer_id", true), 'name'=>'consumer_id', 'id'=>'consumer_id',  'class'=>'h2'))
						echo SMCO_Assistant::wp_droplist_all_consumers_post($post_id, null);
					?>					
				</div>
			<?php
			
		}
		function true_save_goods_batch_box_data($post_id) 
		{
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;		
			update_post_meta($post_id, 'consumer_type',	$_POST['consume_type'.$post_id]);				
			update_post_meta($post_id, 'consumer_id', 	$_POST[$_POST['consume_type'.$post_id].'_consume_'.$post_id]);			
			return $post_id;
		}
		function add_views_column_batch( $columns )
		{
			$columns['consumer_id']						= __("Consumer", "smco");
			return $columns;
		}
		function add_views_sortable_column_batch($sortable_columns){
			$sortable_columns['consumer_id'] 				= 'consumer_id';	
			return $sortable_columns;
		}
		function fill_views_column_batch($column_name, $post_id)
		{
			//$post			= get_post($post_id);
			switch( $column_name) 
			{				
				case 'consumer_id':
					$gt	= get_post_meta( $post_id, 'consumer_id', true );
					if($gt>0)
					{
						$txt		= $this->get_consumer_name( $post_id );
						echo '<img src="'.SMP_URLPATH.'img/check_checked.png" title="'. $txt .'">';
					}
					else
					{
						echo '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
					}
					break;	
			}
		}	
		
		function smp_butch_slide_4($arr, $batch_id)
		{
			$batch			= SMP_Goods_Batch::get_instance($batch_id);
			$count			= $batch->get_meta( "count" );
			$consume_id		= $batch->get_meta( 'consumer_id');
			$consumer_type	= $batch->get_meta( 'consumer_type');
			$post_types		= get_post_types('','object');
			$gg				= '';		
			foreach ($post_types as $post_type ) 
			{
				if(
					$post_type->name == 'nav_menu_item' ||
					$post_type->name == 'revision' ||
					$post_type->name == 'post' ||
					$post_type->name == 'attachment' ||
					$post_type->name == 'goods_type' ||
					$post_type->name == 'smc_currency_type' ||
					$post_type->name == 'location_type' ||
					$post_type->name == SMCO_CONSUME_SCHEME_TYPE
				  )
					continue;
				//echo '<br>'. $post_type->label;
				$gg .= '<input type="radio" class="css-checkbox_black" name="namm" id="'.$post_type->name.'" '.($this->options['supported_types'][$post_type->name] == true ? ' checked="checked"' : '').' />						
				<label class="css-label_black" for="namm">'. $post_type->label. '</label>
				<br>';
			}
			
			$html			= "
			<div class='smp-batch-tab-slide'>
				<div class='smp-table-null' style=''>".
					SMCO_Assistant::wp_droplist_all_consumers($batch_id, $consumer_type, $consume_id, array('name'=>'cons_'.$batch_id))."<br>".
					"<input name='to_consume_".$batch_id."' type='submit'  class='smp-colorized smp-submit'  value='".__("Send batch to Consuming", "smco")."'/>	
				</div>
			</div>";
			return array("title" => __("Consume", "smco"), 	"slide" => $html);
		}
		function get_consumer_name( $batch_id )
		{
			$consumer_type		= get_post_meta($batch_id, 'consumer_type', true);
			if($consumer_type != '' && $consumer_type != 'none')
			{
				switch($consumer_type)
				{
					case 'smp_hub':
					case SMP_FACTORY:
						$consumee	= get_post($id, get_post_meta($batch_id, 'consumer_id', true))->post_title;
						break;
					case "user":
						$consumee	= get_userdata(get_post_meta($batch_id, 'consumer_id', true))->display_name;
						break;
					case SMC_LOCATION_NAME:
						$consumee	= get_term_by($id, get_post_meta($batch_id, 'consumer_id', true), SMC_LOCATION_NAME)->name;
						break;
				}
				return $consumee;
				
			}
		}
		function smp_batch_card_price($text, $batch_id)
		{
			$consume			= '';
			$consumer_type		= get_post_meta($batch_id, 'consumer_type', true);
			if($consumer_type != '' && $consumer_type != 'none')
			{
				$consumee		= $this->get_consumer_name( $batch_id );
				$consume		= "
				<div style='line-height:0.6;'>
					<span style='font-size:0.6em; color:#777;'>" . __("Consumer", 'smco'). '</span> <span style="font-size:0.8em; color:#000;">'.$consumee.'</span>
				</div>';
				
			}
			return $text.$consume;
		}
		function smc_my_location_2($arr, $loc_id)
		{
			return array("title" => __("Consume", "smco"), 	"slide" => $html);
		}
		function add_location_tab($arr, $location)
		{
			global $Ermak_Consume;
			global $Soling_Metagame_Constructor;
			//
			$all_parent_locs_id			= SMC_Location::get_child_location_ids($location->term_id);
			$all_parent_locs_id[]		= $location->term_id;
			$meta_query					= array();
			$meta_query['relation']		= 'AND';
			$meta_query[]				= array(
													'key'	=> 'owner_id',
													'value'	=> $all_parent_locs_id,
													'operator' => "OR"
												);
			$meta_query[]				= array(
													'key'	=> 'dislocation_id',
													'value'	=> $all_parent_locs_id,
													'operator' => "OR"
												);
			/*$meta_query[]				= array(
													'key'	=> 'consumer_type',
													'value'	=> SMC_LOCATION_NAME
												);
			$meta_query[]				= array(
													'key'	=> 'consumer_id',
													'value'	=> $location->term_id
												);*/
			$arg		= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'title',
										'order'     	=> 'ASC',
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',
										'meta_query'	=> $meta_query,
									);
			$all_goods_batchses	= get_posts($arg);
			$bs						= "<h4>".__("Reserves", "smco")."</h4>";
			$bs						.= "<div class=smp-comment style='width:98%;'>". __("For the successful completion of the economic cycle, you have to provide the population with your Locations for all products belonging to the basket of consumption. To ensure that each of them a sufficient number of batches of the corresponding type of product that will belong to this location (in a row, it will show the owner or her daughter locations)..", "smco")."</div><div>"; 
			if(count($all_goods_batchses))
			{
				foreach($all_goods_batchses as $batch_id)
				{
					$gb				= SMP_Goods_Batch::get_instance($batch_id->ID);
					//$bs				.= $GoodsBatch->get_stroke($gb->body, 1);
					$bs				.= $gb->get_stroke($gb->body, 1);
					//$bs					.= $batch_id->ID;
				}
			}
			else
			{
				$bs					.= "<div class=smp-comment_1>". __("No batches presents. This Location no consumed in this circle", "smco")."</div>"; 
			}
			$bs						.= "</div>";
			//
			$reports_text			= "<h4>".__("Reports", "smco")."</h4>";
			$report_id				= SMCO_Report::get_taxonomy_report_id($location, SMC_LOCATION_NAME);
			$report					= get_post($report_id);
			$reports_text			.= $report->post_content;
			
			//
			$loc_data				= SMC_Location::get_term_meta( $location->term_id );//get_option("taxonomy_".$location->term_id);
			$consume_scheme_id		= $loc_data["consume"];	
			
			$requirements			= apply_filters(
														"smco_location_requirement",
														$this->get_location_requipment($location, $consume_scheme_id),
														$location->term_id,
														$loc_data
													);
			
			$consum_content			= apply_filters(
														"smco_location_widget_klapan",
														array
														(
															"title" 	=> '<i class="fa fa-cutlery"></i>', 
															"slide" 	=> $requirements, 
															'hint'		=>__("Requirements", "smco")
														),
														$location->term_id,
														$loc_data
													);
			
			/**/
			
			
			$arr[]					= array("title" => '<i class="fa fa-bar-chart"></i>', 'hint'=>__("Reserves", "smco"), 	"slide" => $bs);
			$arr[]					= array("title" => '<i class="fa fa-briefcase"></i>', 'hint'=>__("Reports", "smco"), 	"slide" => $reports_text);
			$arr[]					= array("title" => '<i class="fa fa-cutlery"></i>', 'hint'=>__("Requirements", "smco"), "slide" => $requirements);
			return $arr;
		}
		
		function get_location_requipment($location, $consume_scheme_id)
		{
			$requirements			= "
			<style>
				#placeholder { width: 320px; height: 280px; display:inline-block; position:relative; float:left;}
				.legend table, .legend > div { height: 60px !important; opacity: 1 !important; left: 210px; top: 10px; width: 160px !important; }
				.legend table {padding: 5px; }
			</style>
			<h3>".__("Requirements", "smco"). "</h3>
			<h5>".__("Itself", "smco")."</h5>".
				$this->admin_consume_form($consume_scheme_id, true).
			"<div style='margin-top:20px;'></div>";
			$charges				= SMCO_Assistant::get_location_charges_id($location->term_id);
			if(count($charges))
			{
				$requirements		.= "<h5>".__("Charges", "smco"). " (" . count($charges) . ")</h5>";
				foreach($charges as $charge)
				{
					$factory_type	= get_post_meta( get_post_meta($charge, "industry", true), 'factory_name', true);
					$charge_cs_id	= get_post_meta($charge, "consume_scheme_id", true);
					$postc			= get_post($charge);
					//$post_type		= $postc->post_type;
					$requirements	.= "<div class='smco_charge'>" .
											$factory_type . 
										" <a href='".get_permalink($charge)."'>" . 
												$postc->post_title . 
										"	</a>
										</div>"
										. $this->admin_consume_form($charge_cs_id, true)
										;				
				}
			}	
			return $requirements;
		}
		function smp_send_batch_changes($batch_id)
		{
			
			if($_POST['to_consume_'.$batch_id])
			{
				//echo $_POST['user_consume_'.$batch_id];
				update_post_meta($batch_id, "consumer_type", $_POST['consume_type'.$batch_id]);
				switch($_POST['consume_type'.$batch_id])
				{
					case SMC_LOCATION_NAME:
						update_post_meta($batch_id, "consumer_id", $_POST['location_consume_'.$batch_id]);
						break;
					case "user":
						update_post_meta($batch_id, "consumer_id", get_current_user_id());
						break;
				}
			}
		}
		function smp_rize_batch($batch_id)
		{
			update_post_meta($batch_id, "consumer_type",	0);
			update_post_meta($batch_id, "consumer_id", 	0);
		}
		function admin_scripts()
		{					
			wp_register_script('smpo_admin', plugins_url( '../js/smpo_admin.js', __FILE__ ), array());
			wp_enqueue_script('smpo_admin');
			
			wp_register_style( 'smpo_admin-style', plugins_url( '../css/smpo_admin.css', __FILE__ ), array());
			wp_enqueue_style ( 'smpo_admin-style' );
		}
		function front_script_add()
		{
			//js
			wp_register_script('smco-front', plugins_url( '../js/smpo_front.js', __FILE__ ), array());
			wp_enqueue_script('smco-front');		
			
			wp_register_style( 'smpo_admin-style', plugins_url( '../css/smpo_admin.css', __FILE__ ), array());
			wp_enqueue_style ( 'smpo_admin-style' );
				
		}
		
		function open_location_content_widget($args, $location_id)		
		{
			$loc_data				= SMC_Location::get_term_meta( $location_id[1] );//get_option("taxonomy_".$location_id[1]);
			$consume_scheme_id		= $loc_data['consume'];
			//$location_type_id		= $Soling_Metagame_Constructor->get_location_type_id($location_id[1]);			
			$html1					.= "
			<div class='klapan3_subtitle lp_clapan_raze'>
				<div style='position:relative; display:block; '>".
					__("Consume", "smco") . 
			'	</div>
				<div class="lp_clapan_raze1"></div>
			</div>';
			
			$new_p					= apply_filters(
														"smco_location_widget_klapan",
														array
														(
															"title" 	=> "<img src=" . SMCo_URLPATH . "img/goods.png>", 
															"slide" 	=> $html1 . "<div class='smco_widget_form smp_bevel_form' style='padding:20px!important; max-width:370px!important; '>" . $this->admin_consume_form($consume_scheme_id, true) . "</div>", 
															'hint' 		=> __("Consume", "smco")
														),
														$location_id,
														$loc_data
													);
			$last					= $args[count($args)-1];
			$args1 					= array_slice  ($args, 0, -1);
			$args1[]				= $new_p;
			$args1[]				= $last;
			return $args1;
		}
		
		function edit_location_type_to_location($term, $tax_name)
		{
			$t_id = $term->term_id;	 
			$term_meta = SMC_Location::get_term_meta( $t_id );//get_option( "taxonomy_$t_id" ); 
			echo apply_filters("smco_edit_location_block", 
			"<tr class='form-field'>
				<th scope='row' valign='top'>
					<label for='term_meta[consume]'>". __('Consume', "smco") . "</label></th>
				</th>
				<td>
					<div class='form-field' style='display:block;'>".
							SMCO_Assistant::wp_drp_all_consumer_schemes( array('selected'=>$term_meta['consume'], 'type'=>SMC_LOCATION_NAME, 'name'=>"term_meta[consume]",'id'=>"term_meta[consume]", 'class'=>'chosen-select')) .	
							"<p class=description>" . __("the volume of goods that is a virtual Locations population consumes in one economic cycle. This is characteristic of traditional society or other non-market economies", "smco"). 
							" - <a href='" . admin_url( 'edit.php?post_type='.SMCO_CONSUME_SCHEME_TYPE ) . "'> See more</a></p>" .
							self::consume_status_picto(SMC_LOCATION_NAME, $t_id) . 
					"</div>	
				</td>
			</tr>", $term, $term_meta);
		}
		
		function smc_location_tab_over_params($text, $location_id, $loc_option)
		{
			$meta		= SMC_Location::get_term_meta($location_id);
			$cons_id	= $meta['consume'];
			return $text . "<div style='padding:5px;'>".$this->admin_consume_form($cons_id, true). "<br>".self::consume_status_picto(SMC_LOCATION_NAME, $location_id, 150, 25) . "</div>";
		}
		
		
		function smp_calc_personal_needs($args)
		{			
			$consume	= get_user_meta(get_current_user_id(), 'consume'); 
			$consume	= $consume[0];			
			return $this->get_cs_table($consume, $location_id, $owner_id);
		}
		function smp_calc_location_needs($args, $location_id)
		{
			$locations	= SMC_Location::get_child_location_ids($location_id);
			$locdata	= SMC_Location::get_term_meta( $location_id );// get_option("taxonomy_$location_id");
			$consume	= $locdata['consume']; 
			return $this->get_cs_table($consume, $locations, $location_id);
		}
		
		function smp_calc_factory_needs($args, $factoty_id)
		{
			$consume	= get_post_meta($factoty_id, "consume_scheme_id", true);
			$owner_id	= get_post_meta($factoty_id, "owner_id", true);
			$locat_id	= get_post_meta($factoty_id, "dislocation_id", true);
			return $this->get_cs_table( $consume, $locat_id, $owner_id );
		}
		
		
		// table for 'Personal Calculator' page
		function get_cs_table($consume_scheme_id, $location_id, $owner_id)
		{
			global $SolingMetagameProduction;
			if(!isset($consume_scheme_id) || $consume_scheme_id == "" || $consume_scheme_id == -1 || $consume_scheme_id == 0 )	return array();
			global $goods_arr;
			$gds		= apply_filters( "smco_get_consumer_need", $this->get_consume_goods($consume_scheme_id), $owner_id, SMC_LOCATION_NAME );
			$scheme		= $this->get_consume_scheme($consume_scheme_id);
			$data		= array();
			$data['name'] = $scheme->post_title;
			$i=0;
			foreach($gds as $gd=>$val)
			{
				if(!$val)	continue;
				$class	= $i++%2 == 0 ? 'ob' :'ab';
				$gt		= SMP_Industry::get_instance($gd);
				$all_gt	= $gt ? $gt->get_goods_array(-1, "ids") : array();
				$arg	= array( 
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'post_type' 	=> GOODS_BATCH_NAME,
									'post_status' 	=> 'publish',
									'fields'		=> 'ids',								
									'meta_query'	=> array(	'relation' 			=> "AND",
																array(
																		'key'		=> 'owner_id',
																		'value'		=> $owner_id,
																		'operator'	=> 'OR'
																	  ),
																	 
																array(
																		'key'		=> 'dislocation_id',
																		'value'		=> $location_id,
																		'operator'	=> 'OR'
																	  ),
																	   /**/	
																array(
																		'key'		=> 'goods_type_id',
																		'value'		=> $all_gt,
																		'operator'	=> 'OR'
																	  ),
															),
								);
				if($SolingMetagameProduction->get_gb_value("best_before"))
				{
					$arg['meta_query'][]	= array(
														'key'		=> 'best_before',
														'value' 	=> 0,
														'compare'	=> '>'
													);
				}
				$gbatchs = get_posts($arg);
				$cnts	 = 0;
				foreach($gbatchs as $gb)
				{
					$cnts += get_post_meta($gb, 'count', true);
				}
				$need	= round($val['goods_value']);
				$enab	= $cnts;
				$nena	= (round($val['goods_value']) - $cnts);
				if($nena<0) $nena = 0;
				$data[$gd]			= array(
					'resorse_name'	=> $gt ? $gt->get('resourse_name') : " -- ",
					'need'			=> $need,
					'evalble'		=> $enab,
					'not_evalble'	=> $nena,
					'consume_scheme_id'=>$consume_scheme_id,
					"money"			=> $need * $val['payments_summ']
				);
				
			}
			return array($data);
		}
		
		function smp_count_circle($current_circle_id)
		{	
			require_once(SMCo_REAL_PATH.'class/SMCO_Consumed_Paymnents.class.php');
			global $SolingMetagameProduction, $SMCO_Consumed_Payments, $RPT;
			$SMCO_Consumed_Payments		= new SMCO_Consumed_Payments();
			ob_start();
			echo "<h2>".sprintf(__("Start consume in %s circle", "smco"), $current_circle_id)."</h2>";			
			$RPT						= "<h2>".sprintf(__("Start consume in %s circle", "smco"), $current_circle_id)."</h2>";
			$supported_types			= $this->options['supported_types'];
			
			foreach($supported_types 	as $supported_type=>$val)
			{
				if(!$val)				continue;
				echo "<HR><H3>$supported_type</H3>";
				
				$RPT					.= "<H3>$supported_type</H3>";
				switch( $supported_type)
				{	
					case 'smp_hub':
					case SMP_FACTORY:
						
						$ar				= array(										
														'numberposts'	=> -1,
														'offset'    	=> 0,
														'orderby'  		=> 'id',
														'post_type' 	=> $supported_type,
														'post_status' 	=> 'publish'
													);
						$factories		= get_posts($ar);
						foreach($factories as $factory)
						{
							//echo $supported_type. ' ' .$factory->ID."<BR>";
							$this->control_consume($factory->ID, $supported_type, $current_circle_id, $i);
						}
						break;
					case SMC_LOCATION_NAME:						
						
						$args					= array(
															 'number' 		=> 0
															,'offset' 		=> 0
															,"hide_empty"	=> false
														);
						$locations				= get_terms(SMC_LOCATION_NAME, $args);
						foreach($locations as $location)
						{
							$this->control_consume($location->term_id, SMC_LOCATION_NAME, $current_circle_id, $i);
						}
						break;
					case "user":	
						
						$users						= get_users();						
						foreach($users as $user)
						{
							$this->control_consume($user->ID, 'user', $current_circle_id, $i);
						}
						break;
					default:
						do_action("smco_count_circle", $current_circle_id, $supported_type, $val);
						break;
				}
							
			}
			echo "<p style='background:darkgreen; color:white;padding:2px 10px;'>pay to ALL CONSUMERER!!!!!!</p>";
			$results = ob_get_contents();
			ob_end_clean();
			$SMCO_Consumed_Payments->pay_all($RPT);
			if(self::save_global_report($current_circle_id, -1, $RPT))
				echo "Successfull";//$results;
			else
				echo "Error!";
		}
		
		function control_consume($id, $consumer_type, $current_circle_id, $i)
		{			
			global $SolingMetagameProduction, $Soling_Metagame_Constructor, $SMCO_Consumed_Payments, $RPT;
			$class						= $i++%2==0 ? "ob":"ab";
			$RPT						.= "<div class='consumer_report_block $class'>";
			$no							= sprintf( __('No goods in this %s Reserve.', 'smco'), $consumer_type );
			/*	
				определяем ответственного выгодопреобретателя (con_id),
				места, откуда можно брать товары (disloc_id),
				отчёт владельца, куда будем писать результаты ($report)
			*/
			switch($consumer_type) 
			{
				case 'user':
					$con_id				= $id;
					$disloc_id			= array();
					$owner_id			= $Soling_Metagame_Constructor->all_user_locations($id);
					$usser				= get_user_by("id", $con_id);
					$name				= $usser->display_name;
					$report				= get_post(SMCO_Report::get_user_report_id( get_userdata($con_id) ));
					break;
				case SMC_LOCATION_NAME:
					$con_id				= $id;
					$disloc_id			= SMC_Location::get_child_location_ids($id, true);
					$owner_id			= $disloc_id;
					$locc				= SMC_Location::get_instance($con_id);
					$name				= $locc->name;
					$report				= get_post(SMCO_Report::get_taxonomy_report_id( SMC_Location::get_instance( $con_id ) ));						
					break;
				case "smc_post":
				case 'smp_hub':
				case SMP_FACTORY:
					$con_id				= get_post_meta($id, 'owner_id', true);
					$disloc_id			= SMC_Location::get_child_location_ids($con_id, true);
					$owner_id			= $disloc_id;
					$obj				= get_post($id);
					$name				= $obj->post_title;
					$report				= get_post(SMCO_Report::get_post_report_id( get_post($con_id), $consumer_type ));
					$consumer_type		= SMC_LOCATION_NAME;
					break;
				default:
					break;				
			}					
			echo '<div style="display:block; color:red; font-size:1.3em; margin-top:10px;padding-top:5px; border-top:1px dotted red;"><b>' . $name . " (id=". $id. '):</b></div>';
			$RPT	.= '<div style="display:block; color:red; font-size:1.3em; margin-top:10px;"><b>' . $name . " (id=". $id. '):</b></div>';
			/*
				подключает объект произвольного типа consumer_type к системе потребления
				отдаёт массив array(
					"con_id"	- term_id Локации-выгодопреобретателя, владельца текущего Потребителя. Или WP_User, на которого можно зарезервировать партию товара (SMP_Goods_batch-объект)
					"disloc_id"	- массив term_id Локаций, которые доступны для потребления, 
					"owner_id"	- устарел, 
					"name"		- заголовок объекта Потребления (для отчётов), 
					"consumer_type" - тип объекта Потребления. Вводите сами и определяете поведение потребления в зависимости от
					"report"	- экземпляр SMCO_Report, который будет ассоциирован с данным объектом (Чтобы показывать историю потребления игроку или админу - это Вам надо будет самому писать)
				);
			*/	
			extract(apply_filters("smco_control_consume", $id, $consumer_type, $current_circle_id));
			//search user's scheme
			$this->do_consume( $con_id, $consumer_type, $disloc_id, $report, $id );
			$RPT		.= "</div>";
		}
		
		
		
		
		/*
			@con_id			- 	Consumerer (SMC_Location instance)
			@consumer_type	-	type of Consumerer's client that eat
			@disloc_id		- 	array of SMC_Locations where locate nead goods batchs
			@report			- 	instance of SMCO_Report to white report
		*/
		function do_consume( $con_id, $consumer_type, $disloc_id, $report, $id, $count=1 )
		{
			global $SolingMetagameProduction, $Soling_Metagame_Constructor, $SMCO_Consumed_Payments, $RPT;
			$consume_scheme_id		= (int)$this->get_user_consume_id($con_id, $consumer_type, $id); 
			$tx						= SMC_Location::get_term_meta($con_id);
			$currency_type_id		= $tx['currency_type'];
			$form					= "<div style='margin-bottom:10px;'>".$this->admin_consume_form($consume_scheme_id, false, $count, null, $currency_type_id). "</div>";
			echo $form;
			
			//echo 'consumer_type <b>'. $consumer_type . "</b> con_id = $con_id<br>";
						
			$user_status			= 0;
			$report_content			= '<BR><h3>'. sprintf(__("Report about consuming in  %d circle", "smco"), $circle_id)."</h3>";
			$report_content			.= "<h4>".__("Current consume scheme is ", "smco") . get_post($consume_scheme_id)->post_title . "</h4>";
			$report_content			.= $form;
			if(
				!isset($consume_scheme_id) || 
				//!isset($report) || 
				$consume_scheme_id<=0
			)	
			{
				echo "no cscheme exists.";
				$RPT					.= "<p>". __("No scheme exists for this consumer", "smco"). "</p>";
				return; // no Consume Scheme exists
			}
			else
			{
				$RPT					.= $form;
			}
			
			$goods_request			= apply_filters(
														"smco_get_consumer_need",
														$this->get_consume_goods($consume_scheme_id),
														$id,
														$consumer_type
													);			
			
			//echo "<p>consume_scheme_id=" . $consume_scheme_id."</p>";
			//var_dump($goods_request);	
			
			//search all goods batchs from user Reserve
			$meta_query				= array(
												'relation' => "AND",
												array(
														'key'		=> 'consumer_type',
														'value'		=> $consumer_type
													  ),
												array(
														'key'		=> 'consumer_id',
														'value'		=> array($con_id, ""),
														'operator' 	=> "OR"
													),/*
												array(
														'key'		=> 'owner_id',
														'value' 	=> $owner_id,
														'operator' 	=> "OR"
													 ),*/
											);
			if($SolingMetagameProduction->is_dislocation() && count($disloc_id))
				$meta_query[]			= array( 'key'	=> 'dislocation_id', 'value'	=> $disloc_id, 'operator' => "OR");
			if($SolingMetagameProduction->get_gb_value("best_before"))
			{			
				$meta_query[]			= 	array(
													'key'		=> 'best_before',
													'value' 	=> 0,
													'compare'	=> '>'
												 );
			}
			$arg					= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'ID',
										'order'     	=> 'ASC',
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',
										'meta_query'	=> $meta_query,
									);					
			$reserved_batchs			= get_posts($arg);
			$RPT						.= "<div class='smc_hidden_comment'><p>".sprintf( __("Reserved %s batches for consumer ID = %s", "smco"), "<b>" . count($reserved_batchs) . "</b>", $con_id ). "</p>";
			foreach($reserved_batchs as $batch_id)
			{
				$batch					= SMP_Goods_Batch::get_instance($batch_id->ID);
				if(!$batch)				
				{
					$RPT				.= "<p style='color:red;'>".sprintf(__("Batch by ID %s not present", "smco"), $batch_id->ID)."</p>";
					continue;
				}
				$owner_id				= $batch->get_meta("owner_id");
				$goods_type_id			= $batch->get_meta("goods_type_id");
				$count1					= $batch->get_meta("count");
				$owner					= SMC_Location::get_instance($owner_id);
				$goods_type				= Goods_Type::get_instance($goods_type_id);
				$RPT					.= "<p>".sprintf(__("Reserved %s units of %s owned by %s. Batch id = %s", "smco"), $count1, "<b>" . $goods_type->post_title . "</b>", $owner->name, $batch_id->ID)."</p>";
			}
			
			/**/
			
			//search all goods batch not reserved		
			$meta_query				= array(
											'relation' => "AND",
											array(
													'key'		=> 'owner_id',
													'value'		=> $con_id
												  )
										);
			if($SolingMetagameProduction->is_dislocation() && $disloc_id)
			{
				$meta_query[]			= array( 'key'	=> 'dislocation_id', 'value' => $disloc_id, 'operator' => "OR");
			}
			if($SolingMetagameProduction->get_gb_value("best_before"))
			{			
				$meta_query[]			= array(
													'key'		=> 'best_before',
													'value' 	=> 0,
													'compare'	=> '>'
												);
			}
			//filter for over plugins
			$meta_query				= apply_filters("smco_consume_batch_filter", $meta_query, $con_id, $disloc_id, $id, $consumer_type); 
			
			$arg					= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'date',
										'order'     	=> 'ASC',
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',										
										'meta_query'	=> $meta_query,
									);
			//echo "<p>" . Assistants::echo_me($meta_query, true) . "</p>";	
			$user_batches_2			= get_posts($arg);
			//
			
			//insertLog("SMCOclass.do_consume", array('meta_query' => $meta_query, ' consumer_type' => $consumer_type, " batches"=>$user_batches_2));
			//
			
			$RPT					.= "<p>".sprintf(__("Consumer have %s batches for extended consume", "smco"), count($user_batches_2))."</p>";
			foreach($user_batches_2 as $batch_id)
			{
				$batch					= SMP_Goods_Batch::get_instance($batch_id->ID);
				if(!$batch)				
				{
					$RPT				.= "<p style='color:red;'>".sprintf(__("Batch by ID %s not present", "smco"), $batch_id->ID)."</p>";
					continue;
				}
				$owner_id				= $batch->get_meta("owner_id");
				$goods_type_id			= $batch->get_meta("goods_type_id");
				$count1					= $batch->get_meta("count");
				$owner					= SMC_Location::get_instance($owner_id);
				$goods_type				= Goods_Type::get_instance($goods_type_id);
				$RPT					.= "
				<p>".
					sprintf(
						__("Location %s present %s units of %s for consume. Batch id = %s", "smco"), 
						"<b>" . $owner->name . "</b>", 
						$count1, 
						"<b>" . $goods_type->post_title . "</b>", 
						$batch_id->ID
					).
				"</p>";
			}
			$RPT						.= "</div>";
			/**/
			
			/*
				вначале потребляем то, что специально зарезервировано игроком, а когда не хватит - всё доступное подряд.
			*/
			$user_batches		= array_merge($reserved_batchs, $user_batches_2);	
			
			//если ничего нет вообще
			if(count($user_batches)==0)
			{
				$report_content 	.= $no;
				$user_status		+= NOTHING_CONSUME_ADD_STATUS;
				$us					= $this->update_consume_status($consumer_type, $con_id, $user_status); 
				$txt				= sprintf(__('No batch to consume. Current change %s status: %s. Now it is %s','smco'), $consumer_type, $user_status, $us);
				$report_content 	.= '<p>'.$txt.'</p>'.
				$report_content		.= SMP_Assistants::get_quality_diagramm($us, 200, 30);		
				$this->set_report($report, $report_content);
				echo "<p>". $txt."</p>";	
				$RPT				.= "<p>". $txt."</p>";
				return;
				/**/
			}
			//если кое-что из необходимого всё-таки есть
			else
			{
				// сортируем по отраслям и потребляем.
				$sorted_batches	= Goods_Batch::sort_batches_by_industries($user_batches);
				foreach($sorted_batches as $sorted_batch=>$val)
				{
					//what need
					$need			= round($goods_request[$sorted_batch]['goods_value']	* $count);
					$payments_summ	= round($goods_request[$sorted_batch]['payments_summ']);//	* $count
					$gt				= SMP_Industry::get_instance($sorted_batch)->get("post_title");
					echo "<p>need=$gt, в наличии партий=". count($val).", value=$need, payments_summ=$payments_summ</p>";
					if($need > 0)
					{
						//continue;
						if ( 
								$nd		= $this->execude_need( 
																array( 'value' => $need, 'industry_type'=>$sorted_batch, 'payments_summ'=>$payments_summ ), 
																$val,
																$report_content, 
																$user_status, 
																$consume_scheme_id,
																$con_id
															) 
							)
						{
							$user_status			= $nd[ 'user_status' ];
							echo "<h2>consumed_value = " . $nd['consumed_value']."</h2>";
							//выплатить если надо владельцу деньги за потребление в соответствии со схемой.
							apply_filters("smco_consumed_by_industry", $nd, $consume_scheme_id, $consumer_type, $con_id, $owner_id);
						}
						else
						{
							continue;
						}
					}
					else
					{
						continue;
					}
								
				}
			}				
			//echo "<BR>---- <BR>";
			echo "<p>". sprintf(__("Change status for %s persents."), $user_status)."</p>";					
			$us						= $this->update_consume_status($consumer_type, $con_id, $user_status);
			
			
			$report_content 		.= '<br>'.sprintf(__('Current change '.$consumer_type.' status: %s. Now it is %s','smco'), $user_status, $us).'<br>';
			$report_content			.= SMP_Assistants::get_quality_diagramm($us, 200, 30);		
			$this->set_report($report, $report_content);
		}
		static function save_global_report($report_id, $start_time, $content)
		{
			global $wpdb;
			return $wpdb->insert(
				$wpdb->prefix.REPORT_DB_PREFIX,
				array(
						"report_id"		=> $report_id,
						"report_type"	=> CONSUME_REPORT_TYPE,
						"start_time"	=> $start_time,
						"content"		=> $content
					 ),
				array( '%d', '%d', '%d', '%s')
			 );									
		}
		
		
		protected function pay($owner_id, $currency_type_id, $count, $price, $industry_type_id)
		{
			if(!self::is_finance())	return;
			global $SMCO_Consumed_Payments;
			echo "<p>". __("Payment", "smp") ."- <b>" . (int)($count * $price) . "</b></p>";			
			$SMCO_Consumed_Payments->pay_summ($owner_id, (int)($count*$price), (int)$currency_type_id);
		}
		
		
		//
		//	@need					- array ("value", "industry_type", "payments_summ")- autopay per 1 unit of consumed goods, that may be payed (see current consume scheme settings)
		//	@batches				- 
		//	@report_content			- text of user report
		//	@user_status			- current object percentage status
		// 	@consume_scheme_id 		- consume scheme id for autopay
		//	$con_id					- owner id
		protected function execude_need( $need, $batches, $report_content, $user_status, $consume_scheme_id, $con_id )
		{
			global $RPT;
			$strt					= (int)($need['value'] );
			$needed_value 			= $strt;
			$comsumed_val			= 0;
			//echo "<p>needed_value = ".count($batches)."</p>";
			foreach($batches as $b)
			{
				$batch				= SMP_Goods_Batch::get_instance($b->ID);
				$currency_type_id	= (int)$batch->get_meta("currency_type_id");
				$ct					= SMP_Currency_Type::get_instance($currency_type_id);
				$owner_id			= $batch->get_post_meta("owner_id");
				$goods_type_id		= $batch->get_post_meta("goods_type_id");
				$gt					= Goods_Type::get_instance($goods_type_id);
				$owner				= SMC_Location::get_instance($owner_id);
				$price				= (int)$batch->get_meta("price");
				$ost				= (int)$batch->rize_batch_delete($needed_value);
				$this->pay($owner_id, $currency_type_id, $ost, $need['payments_summ'], $need['industry_type']);
				$txt				= sprintf(
					__("Consumed %s units in batch #%s (%s) that owned by %s and pay - %s" , "smco"), 
					$ost, 
					$b->ID, 
					"<b>".$gt->post_title."</b>", 
					"<b>".$owner->name."</b>", 
					$ct->get_price($need['payments_summ'] * $ost)
				);
				$RPT				.= "<p>" . $txt . "</p>";
				
				$comsumed_val		+= $ost;
				//echo "  ----  --- ---  отрезано $ost, а всего $comsumed_val"; 
				$needed_value 		-= $ost;
				if($needed_value 	> 0)
				{
					$success		= "<h1>Successfull consume</h1>";
					//$report_content.= $success;
					//echo $success;
				}
				else
				{
					continue;
				}
			}
			if($needed_value <= 0)
			{
				$report_content 	.= sprintf(__("The need for product %s completely satisfied.","smco"), '<b>'.get_post_meta($need['goods_type'], 'resourse_name', true).'</b>').'<BR>';
				$user_status += 5;
			}
			else
			{
				$report_content 	.= sprintf(__("The need in product %s is not completely satisfied. Underconsumption is %s units","smco"), '<b>'.get_post_meta($need['goods_type'], 'resourse_name', true).'</b>', $need_value).'<BR>';
				$user_status -= 2;
			}
			//$RPT		.= "<div style='margin-bottom:1px; padding:5px 10px; background:#EEE; color:#000;'>" . __("change status by", "smco"). " ".$user_status."</div>";
			return array('need'=>$need, 'val'=>$batches, 'report_content'=>$report_content, 'user_status'=>$user_status, "consumed_value"=> $comsumed_val);
		}
		
		/*
			обработчик фильтра после попытки потребить товары определённой отрасли соответствующим Потребителем
			@result_object			- array(
											'need' 			- array ("value", "industry_type"), 
											'val'			- партии товара, подходящие для потребелния
											'report_content'
											'user_status'	 
											'consumed_value'- количество потреблённого товара отрасли @industry_type_id,
											'payments_summ'	- сколько денег надо отдать на счета @owner_id
											)
			@consume_scheme_id
			@consumer_type			- тип потребителя (User, Location, Factory ets.)
			@con_id					- ID потребителя
			@owner_id
		*/
		function smco_consumed_by_industry($result_object, $consume_scheme_id, $consumer_type, $con_id, $owner_id)
		{
			if(!self::is_finance())	return 0;
			
		}
		
		protected function set_report($report=-1, $report_content="")
		{
			if($report==-1 || !isset($report))	return;
			$report->post_content	= $report_content . $report->post_content;				
			wp_update_post(
							array(
									"ID"			=> $report->ID,
									"post_content"	=> $report->post_content
								 )
						  );
		}
		
		function get_consume_status($type, $id)
		{
			switch($type)
			{
				case SMC_LOCATION_NAME:
					$meta					= SMC_Location::get_term_meta( $id );//get_option('taxonomy_$id');
					$user_st				= $meta['user_consume_status'];
					//$user_st				= $user_st[0];
					if(!isset($user_st) || $user_st=='' )		$user_st = 100;
					return $user_st;
				case 'user':
					$user_st				= get_user_meta($id, 'user_consume_status');
					$user_st				= $user_st[0];
					if(!isset($user_st) || $user_st=='' )		$user_st = 100;
					return $user_st;
				case "smc_post":
				case "smp_hub":
				case SMP_FACTORY:
					$user_st				= get_post_meta($id, 'user_consume_status');
					$user_st				= $user_st[0];
					if(!isset($user_st) || $user_st=='' )		$user_st = 100;
					return $user_st;
				default:
					return 0;
			}
		}
		function update_consume_status($type, $id, $add_status)
		{
			switch($type)
			{
				case SMC_LOCATION_NAME:
					$meta					= SMC_Location::get_term_meta( $id );//get_option('taxonomy_$id');
					$user_st				= (int)$this->get_consume_status(SMC_LOCATION_NAME,$id);
					$user_st				+= (int)$add_status;
					$user_st				= $user_st < 0 ? 0 : $user_st;
					$meta['user_consume_status'] = $user_st;
					$meta['last_user_consume_status'] = $add_status;
					SMC_Location::update_taxonomy_custom_meta($id, $meta, false, true);
					return $user_st;
				case 'user':
					$user_st				= $this->get_consume_status('user',$id);
					$user_st				+= $add_status;
					$user_st				= $user_st < 0 ? 0 : $user_st;
					update_user_meta($id, 'last_user_consume_status',	$add_status);
					update_user_meta($id, 'user_consume_status', 		$user_st);
					return $user_st;
				case "smc_post":
				case 'smp_hub':
				case SMP_FACTORY:
					$user_st				= $this->get_consume_status(SMP_FACTORY, $id);
					$user_st				+= $add_status;
					$user_st				= $user_st < 0 ? 0 : $user_st;
					update_user_meta($id, 'last_user_consume_status',	$add_status);
					update_user_meta($id, 'user_consume_status', 		$user_st);
					return $user_st;
				default:
					return 0;
			}
		}
		
		
		function smc_admin_special_pages_choosee($text)
		{
			$posts			= get_posts(array('numberposts' => 500,'post_type'=>"page", 'orderby'=>"ID", 'order'=>'ASC'));
			$new_text		.= '
			<li>
				<h4>'.__("Metagame Consume", "smco").'</h3>
			</li>
			<li>
				<label for="user_consumes_page_ID">'.__("My Consumes", "smp"). " ( shortcode: [smco_user_consume] )".'</label><BR>
				<select  name="user_consumes_page_ID" id="user_consumes_page_ID" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['user_consumes_page_ID'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
			</li>
			
			';
			return $text.$new_text;			
		}
		function smc_admin_sinc_special_pages()
		{
			$this->options['user_consumes_page_ID']				= $_POST['user_consumes_page_ID'];
			$this->update_options();
		}
		function update_options()
		{
			update_option( SMCo, $this->options );
		}	
		
		function smc_myajax_submit($params)
		{
			global $user_iface_color;
			global $Soling_Metagame_Constructor, $SMP_Calculator;
			global $SMP_Currency;
			global $start;
			
			switch($params[0])
			{
					case 'get_current_location_consume':
						if($params[1]==0)
						{
							$consume_form		= '<h3 style="color:white;">'.__("Consume",'smco').': </h3>';
						}
						else
						{	
							$term_data				= SMC_Location::get_term_meta( $params[1] ); //get_option("taxonomy_".$params[1]);
							$consume_scheme_id		= $term_data['consume'];
							$consume_form			= $this->admin_consume_form($consume_scheme_id, true);
						}
						
						$html						.= '<div class="location-tamplate-content" style="display:inline-block;; position:absolute; top:1px; left:10px; width:100%; height:100%;  color:white; font-size:13px;">'.
							//'<h3 style="color:white;">'.__("Consume",'smco').': </h3>'.
							$consume_form .	
						'</div>';
						$d						= array(	
															$params[0], 
															array(
																	'text' 			=> $html,
																	'time'			=> ( getmicrotime()  - $start )	,
																	'cont'			=> $cont
																  )
														);
						$d_obj					= json_encode(apply_filters("smc_ajax_data", $d));
						print $d_obj;
						break;
					case "smco_get_circle_report":
						
						$cont					= $params[1];
						$html					= SMCO_Assistant::get_form($cont);
						$d						= array(	
															$params[0], 
															array(
																	'text' 			=> $html,
																	'time'			=> ( getmicrotime()  - $start )	,
																	'cont'			=> $cont
																  )
														);
						$d_obj					= json_encode(apply_filters("smc_ajax_data", $d));
						print $d_obj;						
						break;	
					case "get_all_location_needs":
						$locs 					= $Soling_Metagame_Constructor->get_locations();
						$html					= "<h2>".__("All Locations needs", "smco"). "</h2>";
						foreach($locs as $location)
						{
							$location_id		= $location->term_id;
							$tx					= SMC_Location::get_term_meta($location_id);
							$currency_type_id 	= $tx['currency_type'];
							$res				= apply_filters( "smp_calc_location_needs",  SMP_Calculator::default_res_table_data(), $location_id ); //$this->smp_calc_location_needs("", $location_id);
							$html				.= "<div class='absaz smp_gt_title' >".$location->name."</div>";
							$html				.= $SMP_Calculator->get_table($res, $location_id, $currency_type_id);
						}
						$d						= array(	
															$params[0], 
															array(
																	'text' 			=> $html,
																	'time'			=> ( getmicrotime()  - $start )
																  )
														);
						$d_obj					= json_encode(apply_filters("smc_ajax_data", $d));
						print $d_obj;						
						break;	
						break;
					
			}

		}
		function smc_myajax_admin_submit($params)
		{
			global $user_iface_color, $wpdb;
			global $Soling_Metagame_Constructor;
			$html			='';
			switch($params[0])
			{
					case "smco_install_data_package":
						switch($params[1])
						{
							case 'fantasy':
							case 'traditional':
							case 'modern':
								break;
						}
						$html			= __("Complete"). ' '. $params[1];			
						$d				= array(	
													$params[0], 
													array(
															'text' 			=> $html,
														 )
												);
						$d_obj			= json_encode(apply_filters("smc_ajax_data", $d));
						print $d_obj;
						break;
					case "update_consume_circle":
						$this->options['consume_interval']		= $params[1];
						update_option(SMCo, $this->options);
						$d				= array(	
													$params[0], 
													array(
															'text' 			=> $params[1],
															'time'			=> ( getmicrotime()  - $start )
														 )
												);
						$d_obj			= json_encode(apply_filters("smc_ajax_data", $d));
						print $d_obj;
						break;
					case "update_consume_circle_id":
						
						$ccid				= $params[1];
						$query				= "DELETE FROM `". $wpdb->prefix.REPORT_DB_PREFIX . "` WHERE report_id>" . $ccid . " AND report_type=" . CONSUME_REPORT_TYPE;
						echo $query;
						$wpdb->get_results($query);
						update_option("current_consume_circle_id", $ccid);
						$d				= array(	
													$params[0], 
													array(
															'text' 			=> $params[1],
															'time'			=> ( getmicrotime()  - $start )
														 )
												);
						$d_obj			= json_encode(apply_filters("smc_ajax_data", $d));
						print $d_obj;
						break;
			}
			
		}
	}

	
?>