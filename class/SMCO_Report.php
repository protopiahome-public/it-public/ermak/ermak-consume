<?php
	class SMCO_Report
	{
		public $id;
		function __construct($id)
		{
			$this->id		= $id;
		}
		static function init()
		{
			
			// Consume Report
			add_action( 'init', 											array(__CLASS__, 'add_cr_type'), 36 );
			add_action( 'save_post_smco_consume_report',					array(__CLASS__, 'true_cr_save_box_data'));
			add_action( 'admin_menu',										array(__CLASS__, 'my_cr_extra_fields'), 6);
			add_filter( 'manage_edit-smco_consume_report_columns',			array(__CLASS__, 'add_cr_views_column'), 4);
			add_filter( 'manage_edit-smco_consume_report_sortable_columns', array(__CLASS__, 'add_cr_views_sortable_column'));
			add_filter( 'manage_smco_consume_report_posts_custom_column', 	array(__CLASS__, 'fill_cr_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php	
			
			
		}
		
		//Consume Report Post Type
		
		
		static function add_cr_type()
		{
			$labels 	= array
			(
				'name' => __('Consume Report', "smco"),
				'singular_name' => __("Consume Report", "smco"), // ����� ������ ��������->�������
				'add_new' => __("add Consume Report", "smco"),
				'add_new_item' => __("add Consume Report", "smco"), // ��������� ���� <title>
				'edit_item' => __("edit Consume Report", "smco"),
				'new_item' => __("add Consume Report", "smco"),
				'all_items' => __("all Consume Reports", "smco"),
				'view_item' => __("view Consume Report", "smco"),
				'search_items' => __("search Consume Report", "smco"),
				'not_found' =>  __("Consume Report not found", "smco"),
				'not_found_in_trash' => __("no found Consume Report in trash", "smco"),
				'menu_name' => __("Consume Report", "smco") // ������ � ���� � �������
			);
			$args 		= array
			(
				'labels' => $labels,
				'public' => true,
				'show_in_nav_menus' => false,
				'show_in_admin_bar' => false,
				'show_ui' => false, // ���������� ��������� � �������
				'has_archive' => false, 
				'exclude_from_search' => true,
				'menu_position' => 5, // ������� � ����
				'show_in_menu' => "Metagame_Consuming_page",
				'supports' => array(  'title', 'editor' )
				,'capability_type' => 'post'
			);
			register_post_type('smco_consume_report', $args);
		}
		
		// ����-���� � ��������		
		static function my_cr_extra_fields() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_cr_fields_box_func'), 'smco_consume_report', 'normal', 'high'  );
		}
		
		static function extra_cr_fields_box_func( $post )
		{		
			global $Soling_Metagame_Constructor;
			$typ			= get_post_meta($post->ID, 'type', 		true);
			if($typ == '')	$typ = 0;
			$user_id		= get_post_meta($post->ID, "user_id", 	true);			
			?>
			<div style='display:inline-block;'>				
				<div style='float:left; position:relative; display:inline-block;'> 
					<div class='h'>	 					
						<label class="h2" for="user_id"><?php  _e("Type of owner", 'smco'); ?></label><br>						
						<input type="radio" id="type_0" name="type" class="css-checkbox" value="0" <?php echo checked(0, $typ); ?>/>	
						<label class="css-label"  for="type_0"><?php _e("User"); ?></label><br>						
						<input type="radio" id="type_1" name="type" class="css-checkbox" value="1" <?php echo checked(1, $typ); ?>/>	
						<label class="css-label"  for="type_1"><?php _e("Location", "smc"); ?></label><br>	
					</div>					
					<div class='h'>	 					
						<label  class="h2" for="user_id"><?php echo __("User"); ?></label><br>						
							<?php
								echo wp_dropdown_users(array("echo"=>false, "selected"=>$user_id, 'class'=>'h2 ', "name"=>"user_id"))
							?>						
					</div>					
					<div class='h'>	 					
						<label  class="h2" for="location_id"><?php echo __("Location"); ?></label><br>						
							<?php
								echo $Soling_Metagame_Constructor->get_location_selecter(array("echo"=>false, "selected"=>$user_id, 'class'=>'h2 ', "name"=>"location_id"))
							?>						
					</div>						
				</div>	
							
			</div>
			
			<?php 
			wp_nonce_field( basename( __FILE__ ), 'consume_report_metabox_nonce' );
		}
		
		static function true_cr_save_box_data ( $post_id) 
		{	
			global $table_prefix;
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['consume_report_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['consume_report_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			/**/
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;				
			update_post_meta($post_id, 'type',		$_POST['type']);	
			switch($_POST['type'])
			{
				case 0:
					$user						= get_user_by ("id", $_POST['user_id']);				
					$post->post_title			= $user->display_name;	
					update_post_meta($post_id, 'user_id',	$_POST['user_id']);	
				case 1:
					$tax						= get_term_by('id', $_POST['location_id'], 'location');
					$post->post_title			= $tax->name;	
					update_post_meta($post_id, 'user_id',	$_POST['location_id']);	
			}	
				
				global $wpdb;
				$wpdb->update(
								$table_prefix."posts",
								array("post_title" => $post->post_title),
								array("ID" => $post_id)
							);
			return $post_id;
		}
		
		
		static function add_cr_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 					=> " ",
				  "IDs"	 				=> __("ID", 'smco'),
				  "title"	 			=> __("Title"),
				  "user_id" 			=> __("User"),
			   );
			return $posts_columns;			
		}
		// ��������� ����������� ����������� �������
		static function add_cr_views_sortable_column($sortable_columns){
			$sortable_columns['IDs'] 					= 'IDs';						
			$sortable_columns['user_id'] 				= 'user_id';			
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_cr_views_column($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{		
				case 'IDs':
					echo $post_id;
					break;		
				case 'user_id':
					echo get_post_meta($post_id, 'user_id', true);
					break;	
			}		
		}
		
		static function get_taxonomy_report_id($taxonomy, $taxonomy_type="")
		{	
			$taxonomy_type=  $taxonomy_type=="" ? "" : SMC_LOCATION_NAME;
			$consume_report		= get_posts(array(
														"numberposts"		=> 1,
														"offset"			=> 0,
														'orderby'  			=> 'title',
														'order'     		=> 'ASC',
														'post_type' 		=> 'smco_consume_report',
														'post_status' 		=> 'publish',
														'meta_query'		=> array(
																						'relation'		=> "AND",
																						array(
																								'key'	=> 'type',
																								'value'	=> 1
																							 ),
																						array(
																								'key'	=> 'user_id',
																								'value'	=> $taxonomy->term_id
																							 ),
																					)
												 )
											);
			if(count($consume_report) == 0)
			{
				$my_post 		= array(
										  'post_title'   		=> $taxonomy->name,
										  'post_type' 			=> 'smco_consume_report',
										  'post_content' 		=> ""  ,
										  'post_status'  		=> 'publish',
										  'comment_status'		=> 'closed',
										);
				$report_id		= wp_insert_post($my_post);
				update_post_meta($report_id, "user_id", $taxonomy->term_id);
				update_post_meta($report_id, "type", 1);
			}
			else
			{
				$report_id		= $consume_report[0]->ID;
			}
			return $report_id;
		}
		static function get_user_report_id($user)
		{
			global $user_r_count;			
			$consume_report		= get_posts(array(
														"numberposts"		=> 1,
														"offset"			=> 0,
														'orderby'  			=> 'title',
														'order'     		=> 'ASC',
														'post_type' 		=> 'smco_consume_report',
														'post_status' 		=> 'publish',
														'meta_query'		=> array(
																						'relation'		=> "AND",
																						array(
																								'key'	=> 'user_id',
																								'value'	=> $user->ID
																							 ),
																						array(
																								'key'	=> 'type',
																								'value'	=> 0
																							 ),
																					)
												 )
											);
			if(count($consume_report) == 0)
			{
				$my_post 		= array(
										  'post_title'   		=> $user->display_name,
										  'post_type' 			=> 'smco_consume_report',
										  'post_content' 		=> ""  ,
										  'post_status'  		=> 'publish',
										  'comment_status'		=> 'closed',
										);
				$report_id		= wp_insert_post($my_post);
				update_post_meta($report_id, "user_id", $user->ID);
				update_post_meta($report_id, "type", 0);
				$user_r_count++;
			}
			else
			{
				$report_id		= $consume_report[0]->ID;
			}
			return $report_id;
		}
		static function get_post_report_id($post, $post_type)
		{
			$consume_report		= get_posts(array(
														"numberposts"		=> 1,
														"offset"			=> 0,
														'orderby'  			=> 'title',
														'order'     		=> 'ASC',
														'post_type' 		=> 'smco_consume_report',
														'post_status' 		=> 'publish',
														'meta_query'		=> array(
																						'relation'		=> "AND",
																						array(
																								'key'	=> $post_type,
																								'value'	=> $user->ID
																							 ),
																						array(
																								'key'	=> 'type',
																								'value'	=> 2
																							 ),
																					)
												 )
											);
			if(count($consume_report) == 0)
			{
				$my_post 		= array(
										  'post_title'   		=> $post->title,
										  'post_type' 			=> 'smco_consume_report',
										  'post_content' 		=> ""  ,
										  'post_status'  		=> 'publish',
										  'comment_status'		=> 'closed',
										);
				$report_id		= wp_insert_post($my_post);
				update_post_meta($report_id, post, $user->ID);
				update_post_meta($report_id, "type", 2);
			}
			else
			{
				$report_id		= $consume_report[0]->ID;
			}
			return $report_id;
		}
		
		public function update($text)
		{
			$report			= get_post_meta($this->id, "report_content", true);
			$report			.= "<BR>" . $text;
			return update_post_meta($this->id, "report_content", $report);		
		}		
	}
?>