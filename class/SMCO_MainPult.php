<?php

	class SMCO_MainPult
	{
		
		static function get_admin_form()
		{
			global $Ermak_Consume;
			$col_arrears	= 0;
			$html			= '<h2>'.__('Metagame Consuming', "smco").'</h2>';
			$html			.= '<h3>'.__("Arrears", "smco").'</h3>
			
			<table class="wp-list-table widefat fixed posts">
				<thread>
					<tr>
						<th scope="col" class="manage-column column-user_name" style="width:300px;">'.__("Player", "smc").'</th>
						<th scope="col" class="manage-column column-user_name" style="width:300px;">'. __('Consume Scheme', "smco").'</th>
						<th scope="col" class="manage-column column-user_name" style="width:100px;">'.__("Last user consume inscrement", "smco").'</th>
						<th scope="col" class="manage-column column-user_name" style="width:100px;">'.__("User consume status", "smco").'</th>
					</tr>
				</thread>
				<tbody>';
			$args = array(
							'blog_id'      	=> $GLOBALS['blog_id'],
							'role'         	=> 'Player',
							'meta_key'		=> 'last_user_consume_status',
							'meta_value'	=> 0,
							'meta_compare'	=> '<',
													
							'orderby' 		=> 'meta_value_num',
							'order'        	=> 'ASC',
							'offset'       	=> '',
							'search'       	=> '',
							'number'       	=> '',
							'count_total'  	=> false,
							'fields'      	=> 'all',
							'who'          	=> ''
						);
			$users			= get_users( $args );
			$cuser			= count($users);
			$col_arrears	+= $cuser;
			if($cuser)
			{
				foreach($users as $user)
				{
					$consume	= get_user_meta($user->ID, 'consume');
					$consume	= $consume[0];
					if($consume)	
						$con	= $Ermak_Consume->admin_consume_form($consume);
					$lucs		= get_user_meta($user->ID, 'last_user_consume_status');
					$ucs		= get_user_meta($user->ID, 'user_consume_status');
					$html		.= '
					<tr class="status-publish hentry alternate iedit author-self level-0" style="border-bottom:1px solid #F1F1F1;">
						<td>' . $user->display_name. "</td>
						<td>" . $con. "</td>
						<td>" . $lucs[0] . "</td>
						<td>" . Ermak_Consume::consume_status_picto($user->ID, "user", 110, 25) . "</td>
					</tr>";
				}
			}
			else
			{
				$html		.= "<tr class='status-publish hentry alternate iedit author-self level-0'><td class=smc_selected_form colspan=4>".__("No element in group", "smco")."</td></tr>";
			}
			$html			.= '
				</tbody>
			</table>
			<div style="margin:30px;"></div>
			<table class="wp-list-table widefat fixed posts">
				<thread>
					<tr>
						<th scope="col" class="manage-column column-user_name" style="width:300px;">'.__("Location", "smc").'</th>
						<th scope="col" class="manage-column column-user_name" style="width:300px;">'. __('Consume Scheme', "smco").'</th>
						<th scope="col" class="manage-column column-user_name" style="width:100px;">'.__("Last user consume inscrement", "smco").'</th>
						<th scope="col" class="manage-column column-user_name" style="width:100px;">'.__("User consume status", "smco").'</th>
					</tr>
				</thread>
				<tbody>';
				
			$locs			= SMC_Location::get_filtered(array('last_user_consume_status'=>"0"), "<");
			$col_arrears	+= count($locs);
			if(count($locs))
			{
				foreach($locs as $loc_id)
				{
					$loc		= SMC_Location::get_instance($loc_id->ID);
					$consume	= $loc_id->consume;
					if($consume)	
						$con	= $Ermak_Consume->admin_consume_form($consume);
					$lucs		= $loc_id->last_user_consume_status;
					$ucs		= $loc_id->user_consume_status;
					$html		.= '
					<tr class="status-publish hentry alternate iedit author-self level-0" style="border-bottom:1px solid #F1F1F1;">
						<td>' . $loc->name. "</td>
						<td>" . $con . "</td>
						<td>" . $lucs . "</td>
						<td>" . Ermak_Consume::consume_status_picto(SMC_LOCATION_NAME, $loc_id->ID, 110, 25) . "</td>
					</tr>";
				}
			}
			else
			{
				$html		.= "<tr class='status-publish hentry alternate iedit author-self level-0'><td class=smc_selected_form colspan=4>".__("No element in group", "smco")."</td></tr>";
			}
			$html			.= '
				</tbody>
			</table>';
			update_option("smco_col_arrears", $col_arrears);
			return $html;
		}
	}
?>