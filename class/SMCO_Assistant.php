<?php

	class SMCO_Assistant
	{
		function __construct()
		{
			
		}
		static function my_admin_bar_render() 
		{
			if(!current_user_can('administrator'))	return;
			global $wp_admin_bar;
			$wp_admin_bar->add_menu( array(
				'parent' => 'metagame', //'false' ��� ��������� ����
							   //��� ID ������� ����
				'id' => 'smco_panel', // ID ������
				'title' => __('Ermak. Consuming', "scmo"), //��������� ������
				'href' => "/wp-admin/admin.php?page=Metagame_Consuming_page" //��� �����	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'smco_panel', //'false' ��� ��������� ����
							   //��� ID ������� ����
				'id' => 'smco_consume_scheme_panel', // ID ������
				'title' => __('all Consume Schemes', 'smco'), //��������� ������
				'href' => "/wp-admin/edit.php?post_type=smco_consume_scheme" //��� �����	
			));
		}
		
		static function get_supported_types($params= null)
		{
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> 'smco_consume_scheme',
									'post_status' 		=> 'publish',									
								);
			if($params['type'])
			{
				$meta			= array(
											'key'		=> "supported_type_name",
											'value'		=> $params['type'],
											'compare'	=> "="
										);
				$args['meta_query'] = array($meta);
			}
			$hubs		= get_posts($args);
			return $hubs;
		}
		static function wp_drp_all_consumer_schemes($params=null)
		{
			$hubs		= self::get_supported_types($params);			
			//var_dump($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			foreach($hubs as $hub)
			{
				if($params['exclude'])
				{
					foreach($params['exclude'] as $x)
					{
						if((int)$ex == $hub->ID)	continue(2);
					}
				}
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		static function wp_users_consumers($params=null, $user_id=-1)
		{
			global $Soling_Metagame_Constructor;
			$params		= isset($params)	? $params : array("name"=>"user_consumers");
			if($user_id == -1)	$user_id 	= get_current_user_id();
			$options						= get_option(SMCo);
			$supported_types				= $options['supported_types'];
			
			$p1					= "
			<div>
				<h3>".__("Consumers","smco")."</h3>";
			$all_locations1		= get_terms('location', array("number"=>0, 'orderby'=>'name', "hide_empty"=>false));
			$all_locations		= array();
			$all_loc_types		= array();
			foreach($all_locations1 as $location)
			{
				if( $Soling_Metagame_Constructor->user_is_owner($location->term_id, $user_id))
				{
					$all_locations[]	= $location;
					$all_loc_types[]	= $Soling_Metagame_Constructor->get_location_type($location->term_id);
				}
			}
			
			
			$p			= "<select ";
			if(isset($params['id']))
				$p		.= 'id="'.$params['id'].'" ';
			if(isset($params['name']))
				$p		.= 'name="'.$params['name'].'" ';
			if(isset($params['class']))
				$p		.= 'class="'.$params['class'].'" ';
			if(isset($params['style']))
				$p		.= 'style='.$params['style'].'" ';		
			if(isset($params['multile']))
				$p		.= 'multiple ';
			$p			.= '><option value="-1"> -- </option>';
			//var_dump($supported_types);
			//var_dump($post_types);
			
			// $taxs		= array("relation"=>"OR");
			// $names		= array();
			foreach($supported_types as $stype=>$val)
			{	
				switch($stype)
				{
					case "user":
						$p			.= '<option value="1" '. selected(1, $params['selected'], false) .'>'.__("My Consumes", "smco").'</option>';
						break;
					case "location":
						$locs		= $Soling_Metagame_Constructor->all_user_locations();
						foreach($locs as $loc)
						{
							$location = get_term_by("id", $loc, "location");
							$lt 	= $Soling_Metagame_Constructor->get_location_type($loc);
							$p		.= "<option value='" . $loc ."' " . selected($loc, $params['selected'], false) . ">". $lt->picto. " ".$lt->post_title. " " . $location->name . "</option>";
						}
						break;
				}			
				//$p							.= "<option value='" . $stype ."' " . selected($stype, $params['selected'], false) . ">". $stype . "</option>";
			}
			
			$p								.= "</select>";
			return $p;  
			
		}
		static function wp_droplist_all_consumers_post($post_id, $params=null)
		{
			$consumer_type					= get_post_meta($post_id, "consumer_type", false);
			$consumer_type					= $consumer_type[0];
			if(!$consumer_type)				$consumer_type = "nothing";
			$consumer_id					= get_post_meta($post_id, "consumer_id", false);
			$consumer_id					= $consumer_id[0];
			return SMCO_Assistant::wp_droplist_all_consumers($post_id, $consumer_type, $consumer_id, $params);
		}
		static function wp_droplist_all_consumers_term($term_id, $tax_name, $params=null)
		{
			switch($tax_name)
			{
				case "location":
					$tdata					= SMC_Location::get_term_meta( $term_id ); //get_option("taxonomy_".$term_id);
					$consumer_type			= $tdata["consumer_type"];
					$consumer_type			= $consumer_type[0];
					if(!$consumer_type)		$consumer_type = "nothing";
					$consumer_id			= $tdata[ "consumer_id"];
					$consumer_id			= $consumer_id[0];
					//return $consumer_id;
					break;
				default:
					return "this Consumer type not present/";
			}
			return SMCO_Assistant::wp_droplist_all_consumers($term_id, $consumer_type, $consumer_id, $params);
		}
		static function wp_droplist_all_consumers($post_id, $consumer_type, $consumer_id, $params=null )
		{
			global $Soling_Metagame_Constructor;
			$params		= isset($params)	? $params : array("name"=>"user_consumers");
			
			
			$options						= get_option(SMCo);
			$supported_types				= $options['supported_types'];
			$p1			=  
			"<div style='margin:0 0 20px 0;'>					
				<input type='radio' value='nothing' id='consume_type".$post_id."_0' name='consume_type".$post_id."' consumer_id='nothing".$post_id."' ". checked("", $consumer_type, false)  ." class1='css-checkbox' chk='$post_id'/> 
				<label for='consume_type".$post_id."_0' class1='css-label'>".__("Nothing", "smco")."</label>
			<br>";
			$p2			= '<div id="nothing'.$post_id.'"></div>';
			foreach($supported_types as $stype=>$val)
			{
				switch($stype)
				{
					case "user":
						$vis		= "user" === $consumer_type ? "block" : "none";
						$p1			.= "						
							<input type='radio' value='user' id='consume_type".$post_id."_1' name='consume_type".$post_id."' consumer_id='user".$post_id."' ". checked("user", $consumer_type, false) ." class1='css-checkbox' chk='$post_id'/> 
							<label for='consume_type".$post_id."_1' class1='css-label'>".__("Player", "smc")."</label>
						<br>";
						$p2			.= "<div style='display:".$vis.";' id='user".$post_id."' class='consume_type styled-select state rounded'>". wp_dropdown_users(array("echo"=>false, "selected"=>$consumer_id, 'class'=>'h2 ', "name"=>"user_consume_".$post_id))."</div>";
						break;
					case "location":
						$vis		= "location" === $consumer_type ? "block" : "none";
						$p1			.= "						
							<input type='radio' value='location' id='consume_type".$post_id."_2' name='consume_type".$post_id."' consumer_id='location".$post_id."' ". checked("location", $consumer_type, false) ." class1='css-checkbox' chk='$post_id'/> 
							<label for='consume_type".$post_id."_2' class1='css-label'>".__("Location", "smc")."</label>
						<br>";
						$p2			.= "<div style='display:".$vis.";' id='location".$post_id."' class='consume_type styled-select state rounded'>".$Soling_Metagame_Constructor->get_location_selecter(array("selected"=>$consumer_id, 'name'=>'location_consume_'.$post_id, 'class'=>''))."</div>";
						break;
				}
					
			}
			//$p								.= "</select>";
			return $p1."</div>".$p2; 
		}
		static function restore_all_statuses()
		{
			$options							= get_option(SMCo);
			$supported_types					= $options['supported_types'];
			foreach($supported_types as $st=>$val)
			{
				switch($st)
				{
					case 'user':
						$args = array(
							'blog_id'      => $GLOBALS['blog_id'],
							'role'         => '',
							'meta_key'     => '',
							'meta_value'   => '',
							'meta_compare' => '',
							'meta_query'   => array(),
							'include'      => array(),
							'exclude'      => array(),
							'orderby'      => 'login',
							'order'        => 'ASC',
							'offset'       => '',
							'search'       => '',
							'number'       => '',
							'count_total'  => false,
							'fields'       => 'all',
							'who'          => '',
							'date_query'   => array() // �������� WP_Date_Query
						);
						$users					= get_users($args);
						$users_count			= 0;
						$user_r_count			= 0;
						foreach($users as $user)
						{
							update_user_meta($user->ID, 'user_consume_status', 100);
						}
						break;
					case "location":
					
						$locs					= get_terms(SMC_LOCATION_NAME, array("number"=>0, 'orderby'=>'name', "hide_empty"=>false, "fields" => "ids"));
						$loc_count				= 0;
						foreach($locs as $id)
						{
							$meta				= SMC_Location::get_term_meta( $id );//get_option("taxonomy_$id");
							$meta['user_consume_status'] = 100;
							//update_option("taxonomy_$id", $meta);
							SMC_Location::update_taxonomy_custom_meta($id, $meta);
						}					
						break;
					case "smp_hub":
					case "factory":
						$args					= array(
												'numberposts'	=> -1,
												'offset'    	=> 0,
												'orderby'  		=> 'id',
												'order'     	=> 'DESC',
												'post_type' 	=> $st,
												'post_status' 	=> 'publish',
												'fields'		=> 'ids',
											);
						$charges				= get_posts($args);
						foreach($charges as $loc)
						{
							 update_post_meta($loc, "user_consume_status", 100);
						}
						break;
				}
			}
		}
		
		static function restore_data()
		{
			global $all_goods_types;
			global $user_r_count;
			$all_goods_types					= Goods_Type::get_global();	
			$help								= '';
			$options							= get_option(SMCo);
			$supported_types					= $options['supported_types'];
			
			
			
			//var_dump($supported_types);
			foreach($supported_types as $st=>$val)
			{
				$args							= array(	
															'numberposts'		=> 1,
															'offset'    		=> 0,
															'orderby'  			=> 'title',
															'order'     		=> 'ASC',
															"post_type"			=> "smco_consume_scheme",
															'meta_query'		=> array(
																							'relation'			=> "OR", 
																							array(
																									'key'		=> "supported_type_name",
																									"value"		=> $st,
																									'compare'	=> "LIKE"
																								 )
																						 )
														);
				$scemes							= get_posts($args);
				//var_dump($scemes);
				//echo "<BR>- ". $scemes[0]->post_tytle;
				
				if(count($scemes)==0)
				{
					$help						.= "<BR>".sprintf(__("There are not preset no one Consume Scheme for %s . Go to <a href='/wp-admin/post-new.php?post_type=smco_consume_report'>this page</a>, create one of Consume Scheme for %s and continue this operation.", "smco"), $st, $st)."<BR>";
					continue;
				}
				switch($st)
				{
					case 'user':
						$users					= get_users();
						$users_count			= 0;
						$user_r_count			= 0;
						foreach($users as $user)
						{
							$consume_sceme		= get_user_meta($user->ID, "consume");
							if($consume_sceme == "" || $consume_sceme == array('-1'))
							{
								update_user_meta($user->ID, 'consume', $scemes[0]->ID);
								$users_count++;
							}
							
							$consume_report		= get_posts(array(
																		"numberposts"		=> -1,
																		"offset"			=> 0,
																		'orderby'  			=> 'title',
																		'order'     		=> 'ASC',
																		'post_type' 		=> 'smco_consume_report',
																		'post_status' 		=> 'publish',
																		'meta_query'		=> array(
																										array(
																												'key'	=> 'user_id',
																												'value'	=> $user->ID
																											 ),
																									)
																 )
															);
							if(count($consume_report) == 0)
							{
								$my_post 		= array(
														  'post_title'   		=> $user->display_name,
														  'post_type' 			=> 'smco_consume_report',
														  'post_content' 		=> ""  ,
														  'post_status'  		=> 'publish',
														  'comment_status'		=> 'closed',
														);
								$report_id		= wp_insert_post($my_post);
								update_post_meta($report_id, "user_id", $user->ID);
								$user_r_count++;
							}
							else
							{
								$report_id		= $consume_report[0]->ID;
							}
							
							
							
						}
						$help					.= "<BR>".sprintf(__("Update %d users", "smpo"), 			$users_count);
						$help					.= "<BR>".sprintf(__("Update %d user's reports", "smpo"), 	$user_r_count);
						/**/
						break;
					case 'location':
						$locs					= get_terms('location', array("number"=>0, 'orderby'=>'name', "hide_empty"=>false, "fields" => "ids"));
						$loc_count				= 0;
						foreach($locs as $loc)
						{
							 $loc_data			= SMC_Location::get_term_meta( $loc );//get_option("taxonomy_".$loc);
							 if(!$loc_data['consume'])
							 {
								$loc_data['consume'] = $scemes[0]->ID;
								update_option("taxonomy_".$loc, $loc_data);
								$loc_count++;
							 }
						}
						$help					.= "<BR>".sprintf(__("Update %d locations", "smpo"), $loc_count);
						break;
					case 'factory':
					case 'smp_hub':
						$loc_count				= 0;
						$args					= array(
												'numberposts'	=> -1,
												'offset'    	=> 0,
												'orderby'  		=> 'id',
												'order'     	=> 'DESC',
												'post_type' 	=> $st,
												'post_status' 	=> 'publish',
												'fields'		=> 'ids',
											);
						$charges				= get_posts($args);
						foreach($charges as $loc)
						{
							 $loc_data			= get_post_meta($loc, "consume_scheme_id", true);
							 if($loc_data == '')
							 {
								update_post_meta($loc, "consume_scheme_id", $scemes[0]->ID);
								$loc_count++;
							 }
						}
						$help					.= "<BR>".sprintf(__("Update %d ".$st, "smpo"), $loc_count);
						break;
				}
			}
			if($help !="")
			{
				echo "<div class='updated below-h2' style='padding:20px!important;'>".$help."</div>";
			}
			//echo "finish";
		}
		
		static function get_all_post_types()
		{
			global $Ermak_Consume;
			$consumer_posts			= array();
			$op						= $Ermak_Consume->options;
			foreach($op['supported_type_types'] as $type=>$val)
			{
				//echo 'type=<b>'.$type.'</b>	, val='.$val.', name='.$op['supported_type_names'][$type].'<BR>';
				if($val=='post' && $op['supported_types'][$type])
				{
					//echo 'type=<b>'.$type.'</b>	, val='.$val.', name='.$op['supported_type_names'][$type].'<BR>';
					$consumer_posts[] = $type;
				}
			}
			return $consumer_posts;
		}
		static function get_location_charges_id($location_id)
		{
			$args					= array(
												'numberposts'	=> -1,
												'offset'    	=> 0,
												'orderby'  		=> 'id',
												'order'     	=> 'DESC',
												'post_type' 	=> self::get_all_post_types(),
												'post_status' 	=> 'publish',
												'fields'		=> 'ids',
												'meta_query' 	=> array(
																			array(
																					"key"		=> "owner_id",
																					"value"		=> $location_id,
																				),
																		),
											);
			$charges				= get_posts($args);
			return $charges;
		}
		
		static function get_all_consume_schemes($filds = 'ids')
		{
			$args					= array(
												'numberposts'	=> -1,
												'offset'    	=> 0,
												'orderby'  		=> 'id',
												'order'     	=> 'DESC',
												'post_type' 	=> 'smco_consume_scheme',
												'post_status' 	=> 'publish',
												'fields'		=> $filds
											);
			$charges				= get_posts($args);
			return $charges;
		}
		
		static function get_cs_map()
		{
			$all_sc					= self::get_all_consume_schemes();
			$scs					= array();
			$final					= array();
			foreach($all_sc as $sc)
			{
				$pr					= get_post_meta($sc, "prevous", true);
				
				if($pr == '')
					$final[$sc]		= array(array($sc, $pr));
				else
					$scs[]			= array($sc, $pr);
			}
			foreach($scs as $sc)
			{
				foreach($scs as $sc1)
				{
					if($sc[1])
					{
						
					}
				}
			}
			return '';
		}
		static function smc_add_object_type($array)
		{
			$smp_industry										= array();
			$smp_industry['t']									= array('type'=>'post');
			$smp_industry['supported_type_name']				= array('type'=>'string');
			$smp_industry['prevous']							= array('type'=>'number');
			$smp_industry['goods']								= array(
																			'type'=>'array', 
																			'goods_type'=>array(
																									'type'=>'id', 
																									'object'=>'smp_industry'
																								),
																			'goods_value'=>'number', 
																			"payments_summ"=>"number"
																		);
			$array[SMCO_CONSUME_SCHEME_TYPE]					= $smp_industry;
			
			$array[SMC_LOCATION_NAME]['consume']				= array('type'=>"id", 'object'=>SMP_CURRENCY_TYPE);
			$array[SMC_LOCATION_NAME]['user_consume_status']	= array('type'=>"number");
			
			
			return $array;
		}
		static function smc_add_option($options)
		{
			$options[SMCo]										= array(  "t" => array("type"=>"option"), "obj_type" => "option", "post_type"=>SMCo, "merge_type"=>CHANGE_OPTION, "data" => get_option(SMCo));
			unset($options[SMCo]['user_consumes_page_ID']);
			$options['current_consume_circle_id']				= array(  "t" => array("type"=>"option"), "obj_type" => "option", "post_type"=>'current_consume_circle_id', "merge_type"=>CHANGE_OPTION, "data" => get_option("current_consume_circle_id"));
			return $options;
		}
		
		static function smp_my_tools($arr)
		{
			$arr[]		= 	array(	"title" => "<div class='smp_tool_icon'><img src='" . SMCo_URLPATH . "img/consuming_ico.png'></div>", 
									"hint"	=> __("Consume circle reports", "smco"), 
									"slide" => "" , 
									"name" 	=> "get_consume_report_btn",
									"exec"	=> "get_consume_circle_report", 
									"args"	=> "" 
								 );
			$arr[]		= 	array(	"title" => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/money_ico.png'></div>", 
									"hint"	=> __("All Locations needs", "smco"), 
									"slide" => "" , 
									"name" 	=> "get_need_locations_btn",
									"exec"	=> "get_all_location_needs", 
									"args"	=> "" 
								 );
			return $arr;
		}
		static function get_form($circle_id="")
		{
			//require_once(SMP_REAL_PATH.'class/Circle_Report.php');
			$cr				= self::get_global_count();
			$circle_id		=	$circle_id=="" ? $cr['current_circle']: $circle_id;
			$rep			= self::get_global($circle_id);	
			$start_time		= (int)$rep[0]["start_time"]  == 0 ? "no present" : date_i18n( __( 'M j, Y - G:i' ), $rep[0]["start_time"]);
			$finish_time	= (int)$rep[0]["finish_time"] == 0 ? "no present" : date_i18n( __( 'M j, Y - G:i' ), $rep[0]["finish_time"]);
			$selector	= "<select name=choose_consume_circle_id style='padding:3px; height:30px; vertical-align:middle;'>";
			for($i=0; $i<=$cr['current_circle']; $i++)
			{
				$selector	.= "<option value=$i ".selected($i, (int)$rep[0]["report_id"] , false) . ">$i</option>";
			}
			$selector		.= "</select>";
			$html		.= "<h1>". sprintf(__("Report from %s consume circle", "smco"), $selector). "</h1>";
			
			if(!count($rep))
			{
				$html		.= "<div class='smp-comment' style='width:98%;'>". __("There are no present report", "smp")."</div>";
			}
			else
			{	
				$html		.= "
				<div class='smp-comment1' style='width:98%;'>".				
					"<table class=adges_table>
						<tr class='ob'>
							<th>".
								__("start time", "smp").   " 
							</th>
							<td>
								<b>" . $start_time. "</b>".
					"		</td>
						</tr>
						<tr class='ab'>
							<th>".
								__("finish time", "smp"). " 
							</th>
							<td>
								<b>" .  $finish_time . "</b>".
					"		</td>
						</tr>
					</table>".
				"</div>";
				$html		.= $rep[0]["content"];
			}
			return $html;
		}
		static function get_global_count()
		{
			global $wpdb;
			return $wpdb->get_row ( "SELECT MAX(report_id) AS current_circle FROM ". $wpdb->prefix.REPORT_DB_PREFIX . " WHERE report_type=".CONSUME_REPORT_TYPE, ARRAY_A );
		}
		static function get_global($circle_id)
		{
			global $wpdb;
			return $wpdb->get_results ( "SELECT * FROM ". $wpdb->prefix.REPORT_DB_PREFIX . " WHERE report_id='".$circle_id."' AND report_type=".CONSUME_REPORT_TYPE , ARRAY_A );
		}
		static function location_display_tbl1($text, $location, $term_meta, $location_type)
		{
			global $Ermak_Consume;	
			$cons_id	= $term_meta['consume'];
			$ct			= $term_meta['currency_type'];
			$html		= "
			<div class='cell_011'>
				<div style='font-size:11px; color:#EEE; font-family:Open Sans, Arial; display:inline-block; position:relative; background:#444; width:100%'>
					<div style='text-transform: uppercase; padding:3px 10px; background-color:#111; font-size:17px;'>".
						__("Location's amortization costs", "smco"). 
					"</div>".
					$Ermak_Consume->admin_consume_form($cons_id, true, 1, false, $ct).
					"<div style='text-transform: uppercase; padding:3px 10px; background-color:#111; font-size:17px;'>".
						__("Consume status", "smco"). 
					"</div>
					<div style='display:inline-block; position:relative; margin:15px; 3px'>".
						Ermak_Consume::consume_status_picto(SMC_LOCATION_NAME, $location->term_id, 300, 25) .
				"	</div>
				</div>
			</div>";
			return $text.$html;
			
		}
		static function ermak_booker_mobile_admin($text)
		{
			if(!is_plugin_active('Ermak_Booker/Ermsk_Booker.php')) 	return;
			$checked1	= in_array(EB_PRODUCTS_CONSUME_PAGE,		Ermak_Booker_Plugin::get_instance()->options['mobile_slides']);
			$html		= "
			<p>
				<input type='checkbox' class='checkbox' name='pr_consume' id='pr_consume' value='".EB_PRODUCTS_CONSUME_PAGE."' ".checked($checked1, 1, 0) . "/>
				<label for='pr_consume'>".__("Consume", "smco"). "</label>
				<div id='_pr_consume' class='h4 " . ( !$checked1 ? "lp-hide" : "" ) . "'>
					<board_title>".__("Consume", "smco"). "</board_title>
					<p class='smc-description'>".__("This inset hasn't settings.", ERMAK_BOOKER)."</p>
				</div>
			</p>";
			return $text . $html;
		}
		static function ermak_booker_mobile_admin_save()
		{
			if(!is_plugin_active('Ermak_Booker/Ermsk_Booker.php')) 	return;	
			//echo "<BR>".$_POST['pr_consume']."<BR>";
			if($_POST['pr_consume'])		Ermak_Booker_Plugin::get_instance()->options['mobile_slides'][]	= $_POST['pr_consume'];
		}
		static function ermak_booker_mobile_slide($arr, $slide_id, $loc_id)
		{
			if(!is_plugin_active('Ermak_Booker/Ermsk_Booker.php')) 	return $arr;	
			return array(__("Consume", "smco"), "", "#003300");
		}
	}
?>