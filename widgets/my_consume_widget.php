<?php 
	
	
	class SMCO_My_Consume_Widget extends WP_Widget
	{
		
		/**
		 * Constructor
		 *
		 * @return void
		 * @since 0.5.0
		 */
		function SMCO_My_Consume_Widget()
		{
			parent::__construct( false, __('Ermak Consume Widget', 'smco'), array('description' => __("Player's consume views", 'smc'), 'classname' => 'smco_my_consume') );
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $args
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function widget( $args, $instance ) 
		{
			global $Ermak_Consume;
			extract( $args );
			$instance['title'] ? NULL : $instance['title'] = '';
			$title = apply_filters('widget_title', $instance['title']);
			$output = $before_widget."\n";
			if($title)
				$output .= $before_title.$title.$after_title;
			$consume = get_user_meta(get_current_user_id(), 'consume'); 
			$consume = $consume[0];
			$output .= $Ermak_Consume->admin_consume_form($consume, true);
			$output .=  Ermak_Consume::consume_status_picto("user", get_current_user_id(), 210);
			$output .= $after_widget."\n";
			echo $output;
		}
		
		/**
		 * Configuration form.
		 *
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function form( $instance ) 
		{
			$defaults = array(
				'title' 			=> __("My Consumes", "smco"),		
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			?>				
				<div class="alx-options-posts">
					<p>
						<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label>
						<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
					</p>		 
				</div>
			<?php
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $instance
		 * @return array
		 * @since 1.5.6
		 */
		function update( $new_instance, $old_instance )
		{
			$instance = $old_instance;
			$instance['title'] 			= strip_tags($new_instance['title']);		
			return $instance;
		}
	}
	
/*  Register widget
/* ------------------------------------ */
	function smco_register_widget() { 
		register_widget( 'SMCO_My_Consume_Widget' );
	}
	add_action( 'widgets_init', 'smco_register_widget' );
	

